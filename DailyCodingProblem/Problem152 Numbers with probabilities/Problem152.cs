using System;

namespace DailyCodingProblem.Problem152_Numbers_with_probabilities
{
    public class Problem152
    {
        private readonly Random _random = new Random();
        
        public int Generate(int[] numbers, float[] weights)
        {
            if (numbers.Length == 0)
                return 0;

            if (numbers.Length != weights.Length)
                return 0;
            
            var seed = _random.NextDouble();
            var lowerLimit = 0.0;
            var upperLimit = weights[0];
            var lastIndex = numbers.Length - 1;

            for (var i = 0; i < lastIndex; i++)
            {
                if (seed >= lowerLimit && 
                    seed < upperLimit)
                {
                    return numbers[i];
                }

                lowerLimit += weights[i];
                upperLimit += weights[i + 1];
            }

            return numbers[lastIndex];
        }
    }
}