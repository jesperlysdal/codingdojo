using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace DailyCodingProblem.Problem152_Numbers_with_probabilities
{
    [TestFixture]
    public class Problem152UT
    {
        [Test]
        public void test()
        {
            var sut = new Problem152();

            var outcomes = new List<int>();

            for (var i = 0; i < 10000; i++)
            {
                var number = sut.Generate(new[] {1, 2, 3, 4}, new[] {0.1f, 0.5f, 0.2f, 0.2f});
                outcomes.Add(number);
            }

            var result = outcomes
                .GroupBy(e => e)
                .Select(e => (e.Key, e.Count()))
                .OrderByDescending(e => e.Item2)
                .ToArray();
            
            Assert.AreEqual(5000,result[0].Item2, 100);
            Assert.AreEqual(2000,result[1].Item2, 100);
            Assert.AreEqual(2000,result[2].Item2, 100);
            Assert.AreEqual(1000,result[3].Item2, 100);
        }
    }
}