//For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24].
//If our input was [3, 2, 1], the expected output would be [2, 3, 6].

using NUnit.Framework;

namespace DailyCodingProblem.Problem002
{
    [TestFixture]
    public class Problem02UT
    {
        [Test]
        [TestCase(new [] {1, 2, 3, 4, 5}, new [] {120, 60, 40, 30, 24})]
        [TestCase(new [] {3, 2, 1}, new [] {2, 3, 6})]
        public void test_product_array(int[] input, int[] expected)
        {
            var result = Problem002.ToProductArray(input);
            
//            Assert.AreEqual(expected: expected, result);
        }
    }
}