//Given an array of integers, return a new array such that each element at index i of the new array
//is the product of all the numbers in the original array except the one at i.
//For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24].
//If our input was [3, 2, 1], the expected output would be [2, 3, 6].
//Follow-up: what if you can't use division?

using System.Collections.Generic;
using System.Linq;

namespace DailyCodingProblem.Problem002
{
    public class Problem002 : IProblem
    {
        public int Problem { get; } = 2;
        
        public static int[] ToProductArray(int[] input)
        {
            if (input.Length < 3)
                return input;

            var result = new List<int>(input);

            for (var i = 0; i < input.Length; i++)
            {
                var temp = input.ToList();
                temp.RemoveAt(i);
                result[i] = temp.Aggregate(1, (current, number) => current * number);
            }

            return result.ToArray();
        }
    }
}