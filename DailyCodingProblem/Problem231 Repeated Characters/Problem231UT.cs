﻿using NUnit.Framework;

namespace DailyCodingProblem.Problem231_Repeated_Characters
{
    [TestFixture]
    public class Problem231UT
    {
        [Test]
        [TestCase("aaabbc", "ababac")]
        [TestCase("aaab", "None")]
        public void test(string input, string expected)
        {
            var sut = new Problem231();

            var result = sut.Rearrange(input);
            
            Assert.AreEqual(expected, result);
        }
    }
}