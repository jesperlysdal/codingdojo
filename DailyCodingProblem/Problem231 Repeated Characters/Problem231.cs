﻿//Given a string with repeated characters, rearrange the string so that no two adjacent characters are the same.
//If this is not possible, return None.
//For example, given "aaabbc", you should return "ababac". Given "aaab", return None.

using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DailyCodingProblem.Problem231_Repeated_Characters
{
    public class Problem231
    {
        public string Rearrange(string input)
        {
            if (string.IsNullOrEmpty(input))
                return "None";
            
            var letterDic = new Dictionary<char, int>().ToLookup(e => e.Key);
            
            foreach (var letter in input)
            {
                    //letterDic[letter] += letter;
                
            }
            var mostOccuring = letterDic.First();
            var rest = letterDic.Where(e => e.Key != mostOccuring.Key).ToList();

            var limit = input.Length % 2 == 0 ? 2 : 3;
            //if (mostOccuring.Value > input.Length - 2)
            //    return "None";
            
            var sb = new StringBuilder();

            var counter = 0;
            for (var i = 0; i < input.Length; i++)
            {
                if (i % 2 == 0)
                {
                    sb.Append(mostOccuring.Key);
                }
                else
                {
                    sb.Append(rest[counter].Key);
                    counter++;
                }
            }

            return sb.ToString();
        }
    }
}