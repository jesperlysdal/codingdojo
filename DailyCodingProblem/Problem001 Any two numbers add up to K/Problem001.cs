//Given a list of numbers and a number k, return whether any two numbers from the list add up to k.
//For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.
//Bonus: Can you do this in one pass?

using System.Collections.Generic;

namespace DailyCodingProblem.Problem001
{
    public class Problem001 : IProblem
    {
        public int Problem { get; } = 1;
        
        public static bool AnyTwoNumbersAddsUpTo(int[] input, int k)
        {
            var mem = new Dictionary<int, bool> {{input[0], true}};

            for (var i = 1; i < input.Length; i++)
            {
                if (mem.ContainsKey(k - input[i]))
                {
                    return true;
                }
                mem.Add(input[i], true);
            }

            return false;
        }
    }
}