//For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.

using NUnit.Framework;

namespace DailyCodingProblem.Problem001
{
    [TestFixture]
    public class Problem01UT
    {
        [Test]
        [TestCase(new[]{10, 15, 3, 7}, 17, true)]
        [TestCase(new[]{11, 15, 3, 7}, 17, false)]
        public void test_two_numbers_adds_up_to_k(int[] input, int k, bool expected)
        {
            var result = Problem001.AnyTwoNumbersAddsUpTo(input, k);
            
            Assert.AreEqual(expected, result);
        }
    }
}