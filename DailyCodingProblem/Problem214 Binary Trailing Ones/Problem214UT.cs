using NUnit.Framework;

namespace DailyCodingProblem.Problem214
{
    [TestFixture]
    public class Problem214UT
    {
        public void test()
        {
            var result = Problem214.Calculate(156);
            
            Assert.AreEqual(3, result);
        }
    }
}