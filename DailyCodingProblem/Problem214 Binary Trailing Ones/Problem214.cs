//Given an integer n, return the length of the longest consecutive run of 1s in its binary representation.
//
//For example, given 156, you should return 3.

using System;
using System.Collections.Generic;
using System.Linq;

namespace DailyCodingProblem.Problem214
{
    public static class Problem214
    {
        public static int Calculate(int input)
        {
            if (input <= 0)
                return 0;
            
            var binaryNumber = Convert.ToString(input, 2);

            var trailingOnes = binaryNumber.TrailingOnes();
            
            return trailingOnes
                .OrderByDescending(s => s.Length)
                .First()
                .Length;
        }

        private static List<string> TrailingOnes(this string input)
        {
            return input
                .Split('0')
                .ToList();
        }
    }
}