//Given a string of round, curly, and square open and closing brackets,
//return whether the brackets are balanced (well-formed).
//For example, given the string "([])[]({})", you should return true.
//Given the string "([)]" or "((()", you should return false.

namespace DailyCodingProblem.Problem027
{
    public class Problem027 : IProblem
    {
        public int Problem { get; } = 27;
    }
}