// Implement a job scheduler which takes in a function f and an integer n, and calls f after n milliseconds.

using System;
using System.Threading.Tasks;

namespace DailyCodingProblem.Problem010_Job_Scheduler
{
    public class Problem010
    {
        public async Task Enqueue(Action function, int milliseconds)
        {
            await Task.Delay(milliseconds);
            function.Invoke();
        }

        public void Enqueue(Func<int, int> function, int milliseconds)
        {
            // function.Invoke();
        }
    }
}