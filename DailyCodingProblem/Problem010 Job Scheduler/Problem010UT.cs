using System;
using System.Diagnostics;
using System.Threading.Tasks;
using NUnit.Framework;

namespace DailyCodingProblem.Problem010_Job_Scheduler
{
    [TestFixture]
    public class Problem010UT
    {
        public void DoStuff()
        {
            Console.WriteLine("Hello world!");
        }
        
        [Test]
        public async Task test()
        {
            var sut = new Problem010();

            var sw = new Stopwatch();
            sw.Start();
            await sut.Enqueue(DoStuff, 5000);
            var elapsed = sw.Elapsed.Seconds;
            Assert.True(elapsed >= 5);
        }
    }
}