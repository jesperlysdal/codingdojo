//Given a 32-bit integer, return the number with its bits reversed.
//For example, given the binary number 1111 0000 1111 0000 1111 0000 1111 0000, return 0000 1111 0000 1111 0000 1111 0000 1111.

using System;

namespace DailyCodingProblem.Problem161
{
    public class Problem161 : IProblem
    {
        public int Problem { get; } = 161;
        
        public static string ToReverseBinaryNumber(int input)
        {
            return Convert.ToString(input, 2)
                .Replace('1', 'x')
                .Replace('0', '1')
                .Replace('x', '0');
        }
    }
}