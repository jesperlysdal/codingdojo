using NUnit.Framework;

namespace DailyCodingProblem.Problem161
{
    [TestFixture]
    public class Problem161UT
    {
        [Test]
        [TestCase(8, "0111")]
        public void test(int input, string expected)
        {
            var result = Problem161.ToReverseBinaryNumber(input);
            
            Assert.AreEqual(expected, result);
        }
    }
}