namespace DailyCodingProblem.Problem171
{
    public enum EntryType
    {
        Enter = 0,
        Exit = 1
    }
}