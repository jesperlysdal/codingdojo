//{"timestamp": 1526579928, count: 3, "type": "enter"}

namespace DailyCodingProblem.Problem171
{
    public class EntryModel
    {
        public string TimeStamp { get; set; }
        public int Count { get; set; }
        public EntryType Type { get; set; }
    }
}