//You are given a list of data entries that represent entries and exits of groups of people into a building.
//An entry looks like this:
//{"timestamp": 1526579928, count: 3, "type": "enter"}
//This means 3 people entered the building. An exit looks like this:
//{"timestamp": 1526580382, count: 2, "type": "exit"}
//This means that 2 people exited the building. timestamp is in Unix time.
//Find the busiest period in the building, that is, the time with the most people in the building.
//Return it as a pair of (start, end) timestamps.
//You can assume the building always starts off and ends up empty, i.e. with 0 people inside.

using System;
using NUnit.Framework;

namespace DailyCodingProblem.Problem171
{
    [TestFixture]
    public class Problem171UT
    {
        private static EntryModel[] _entries;
        private static (string, string) _expected;
        
        [SetUp]
        public void SetUp()
        {
            _entries = new []
            {
                new EntryModel() {TimeStamp = "0800", Type = EntryType.Enter, Count = 3}, 
                new EntryModel() {TimeStamp = "0830", Type = EntryType.Enter, Count = 2}, 
                new EntryModel() {TimeStamp = "0900", Type = EntryType.Exit, Count = 1}, 
                new EntryModel() {TimeStamp = "1130", Type = EntryType.Enter, Count = 5}, 
                new EntryModel() {TimeStamp = "1630", Type = EntryType.Exit, Count = 7}
            };
            _expected = ("1130", "1630");
        }
        
        [Test]
        public void test_busiest_period()
        {
            var result = Problem171.BusiestPeriod(_entries);

            Assert.AreEqual(expected: _expected, actual: result);
        }
    }
}