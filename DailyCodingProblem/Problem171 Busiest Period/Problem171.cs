//You are given a list of data entries that represent entries and exits of groups of people into a building.
//An entry looks like this:
//{"timestamp": 1526579928, count: 3, "type": "enter"}
//This means 3 people entered the building. An exit looks like this:
//{"timestamp": 1526580382, count: 2, "type": "exit"}
//This means that 2 people exited the building. timestamp is in Unix time.
//Find the busiest period in the building, that is, the time with the most people in the building.
//Return it as a pair of (start, end) timestamps.
//You can assume the building always starts off and ends up empty, i.e. with 0 people inside.

namespace DailyCodingProblem.Problem171
{
    public class Problem171 : IProblem
    {
        public int Problem { get; } = 171;
        
        public static (string, string) BusiestPeriod(EntryModel[] entries)
        {
            var peopleInside = 0;
            var highestAmount = 0;
            var busiestPeriod = ("", "");

            for (var i = 0; i < entries.Length; i++)
            {
                if (entries[i].Type == EntryType.Enter)
                    peopleInside += entries[i].Count;
                else
                    peopleInside -= entries[i].Count;

                if (peopleInside <= highestAmount) 
                    continue;
                
                highestAmount = peopleInside;
                
                //This works for edge cases, since we can assume the building always starts off and ends up empty,
                //meaning the last entry will never be the busiest period.
                busiestPeriod = (entries[i].TimeStamp, entries[i + 1].TimeStamp);
            }

            return busiestPeriod;
        }
    }
}