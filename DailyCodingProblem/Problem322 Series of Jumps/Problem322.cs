﻿//Starting from 0 on a number line, you would like to make a series of jumps that lead to the integer N.
//On the ith jump, you may move exactly i places to the left or right.
//Find a path with the fewest number of jumps required to get from 0 to N.

using System;
using System.Collections.Generic;

namespace DailyCodingProblem.Problem322_Series_of_Jumps
{
    public class Problem322
    {
        public Result FindShortestPath(int target)
        {
            var currentPosition = 0;
            var result = new Result(currentPosition, new List<int>());
            var i = 0;

            while (currentPosition != target)
            {
                i++;

                if (currentPosition < target)
                    currentPosition += i;
                else
                    currentPosition -= i;
                
                result.Route[Math.Abs(i) - 1] = currentPosition;
            }

            result.Jumps = currentPosition;
            return result;
        }
    }
}