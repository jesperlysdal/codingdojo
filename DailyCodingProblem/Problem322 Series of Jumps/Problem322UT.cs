﻿using System;
using System.Linq;
using NUnit.Framework;

namespace DailyCodingProblem.Problem322_Series_of_Jumps
{
    [TestFixture]
    public class Problem322UT
    {
        [Test]
        [TestCase(3, 2)]
        [TestCase(4, 3)]
        public void test(int target, int expected)
        {
            var sut = new Problem322();

            var result = sut.FindShortestPath(target);
            
            Console.WriteLine($"Route to target {target} took {result.Jumps} moves. The route was {result.Route.Select(e => string.Join(", ", e))}");
            Assert.AreEqual(expected, result.Jumps);
        }
    }
}