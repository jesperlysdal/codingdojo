﻿using System.Collections.Generic;

namespace DailyCodingProblem.Problem322_Series_of_Jumps
{
    public class Result
    {
        public Result(int jumps, List<int> route)
        {
            Jumps = jumps;
            Route = route;
        }
        
        public int Jumps { get; set; }
        public List<int> Route { get; set; }
    }
}