//Given a list of words, return the shortest unique prefix of each word. For example, given the list:
//•	dog
//•	cat
//•	apple
//•	apricot
//•	fish
//Return the list:
//•	d
//•	c
//•	app
//•	apr
//•	f

using System.Collections.Generic;
using System.Linq;

namespace DailyCodingProblem.Problem162
{
    public class Problem162 : IProblem
    {
        public int Problem { get; } = 162;
        
        public List<string> ToUniquePrefixes(List<string> input)
        {
            var prefixes = new Dictionary<string, string>();
            foreach (var word in input)
            {
                if (prefixes.ContainsValue(word))
                    continue;
                
                if (!prefixes.TryGetValue(word[0].ToString(), out var temp))
                {
                    prefixes.Add(word[0].ToString(), word);
                }
                else
                {
                    var existingWord = prefixes[word[0].ToString()];

                    var i = 0;

                    while (existingWord[i] == word[i])
                    {
                        i++;
                    }

                    prefixes.Remove(word[0].ToString());

                    prefixes[new string(existingWord.Take(i + 1).ToArray())] = existingWord;
                    prefixes.Add(new string(word.Take(i + 1).ToArray()), word);
                }
            }

            return prefixes.Select(e => e.Key).ToList();
        }
    }
}