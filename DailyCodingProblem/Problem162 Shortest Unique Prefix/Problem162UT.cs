//Given a list of words, return the shortest unique prefix of each word. For example, given the list:
//•	dog
//•	cat
//•	apple
//•	apricot
//•	fish
//Return the list:
//•	d
//•	c
//•	app
//•	apr
//•	f

using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace DailyCodingProblem.Problem162
{
    [TestFixture]
    public class Problem162UT
    {
        private static readonly List<string> Testlist = new List<string> {"dog", "cat", "apple", "apricot", "fish"};
        
        [Test]
        [TestCase(new []{"dog", "cat", "apple", "apricot", "fish"}, new []{"d", "c", "app", "apr", "f"})]
        [TestCase(new []{"dog", "cat", "apple", "apricot", "appelsin", "fish"}, new []{"d", "c", "appl", "apr", "appe", "f"})]
        public void test(string[] input, string[] expectedOutput)
        {
            var sut = new Problem162();
            var result = sut.ToUniquePrefixes(input.ToList());
            
            Assert.AreEqual(expectedOutput, result);
        }
    }
}