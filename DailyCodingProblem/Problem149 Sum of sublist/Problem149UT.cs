using NUnit.Framework;

namespace DailyCodingProblem.Problem149_Sum_of_sublist
{
    [TestFixture]
    public class Problem149UT
    {
        [Test]
        public void test()
        {
            var result = new[] {1, 2, 3, 4, 5}.SumOfSublist(sublist: (1, 3));
            
            Assert.AreEqual(5, result);
        }
    }
}