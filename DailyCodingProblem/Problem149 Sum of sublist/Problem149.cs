// Given a list of numbers L, implement a method sum(i, j) which returns the sum from the sublist L[i:j] (including i, excluding j).
// For example, given L = [1, 2, 3, 4, 5], sum(1, 3) should return sum([2, 3]), which is 5.
// You can assume that you can do some pre-processing. sum() should be optimized over the pre-processing step.

using System.Collections.Generic;
using System.Linq;

namespace DailyCodingProblem.Problem149_Sum_of_sublist
{
    public static class Problem149
    {
        public static int SumOfSublist(this IEnumerable<int> input, (int, int) sublist)
        {
            return input
                .Skip(sublist.Item1)
                .Take(sublist.Item2 - sublist.Item1)
                .Sum();
        }
    }
}