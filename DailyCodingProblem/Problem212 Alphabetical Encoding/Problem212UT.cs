using NUnit.Framework;

namespace DailyCodingProblem.Problem212
{
    [TestFixture]
    public class Problem212UT
    {
        [Test]
//        [TestCase(26, "Z")]
        [TestCase(27, "AA")]
        [TestCase(28, "AB")]
        [TestCase(52, "BA")]
//        [TestCase(10000, "AAA")] TODO: only supporting from "AA" up to "ZZ" for now
        public void test(int input, string expected)
        {
            var sut = new Problem212();

            var result = sut.AlphabeticalEncoding(input);
            
            Assert.AreEqual(expected, result);
        }
    }
}