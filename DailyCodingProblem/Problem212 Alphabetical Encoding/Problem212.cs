//Spreadsheets often use this alphabetical encoding for its columns: "A", "B", "C", ..., "AA", "AB", ..., "ZZ", "AAA", "AAB", ....
//
//Given a column number, return its alphabetical column id. For example, given 1, return "A". Given 27, return "AA".

namespace DailyCodingProblem.Problem212
{
    public class Problem212
    {
        public string AlphabeticalEncoding(int input)
        {
            if (input <= 0)
                return string.Empty;
            
            var a = input / 26;
            var b = input % 26;

            var firstPart = (char) (a > 0 ? a + 64 : 65);
            var secondPart = (char) (b > 0 ? b + 64 : 65);

            return firstPart.ToString() + secondPart;
        }
    }
}