using System;
using NUnit.Framework;

namespace DailyCodingProblem.Problem054
{
    [TestFixture]
    public class Problem054UT
    {
        private char[,] board;
        
        [SetUp]
        public void SetUp()
        {
            board = new [,]{
                {'.','.','.','7','.','.','3','.','1'},
                {'3','.','.','9','.','.','.','.','.'},
                {'.','4','.','3','1','.','2','.','.'},
                {'.','6','.','4','.','.','5','.','.'},
                {'.','.','.','.','.','.','.','.','.'},
                {'.','.','1','.','.','8','.','4','.'},
                {'.','.','6','.','2','1','.','5','.'},
                {'.','.','.','.','.','9','.','.','8'},
                {'8','.','5','.','.','4','.','.','.'}};
        }
        
        [Test]
        public void test_solve_sudoku()
        {
            var sut = new Problem054();

            var result = sut.SolveSudoku(board);
            
            Print2DArray(result.Board);

            var test = 1;
        }
        
        public static void Print2DArray<T>(T[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i,j] + "\t");
                }
                Console.WriteLine();
            }
        }
    }
}