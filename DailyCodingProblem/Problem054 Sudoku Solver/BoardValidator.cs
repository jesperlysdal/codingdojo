using System;

namespace DailyCodingProblem.Problem054
{
    public static class BoardValidator
    {
        public static bool Validate(char[,] grid)
        {
            for (var i = 0; i < 9; i++)
            {
                var row = new bool[10];
                var col = new bool[10];

                for (var j = 0; j < 9; j++)
                {
                    if(row[grid[i,j]] && grid[i, j] > 0)
                    {
                        return false;
                    }
                    row[grid[i, j]] = true;

                    if (col[grid[j, i]] && grid[j, i] > 0)
                    {
                        return false;
                    }
                    
                    col[grid[j, i]] = true;

                    if ((i + 3) % 3 == 0 && (j + 3) % 3 == 0)
                    {
                        var sqr = new bool[10];
                        for (var m = i; m < i + 3; m++)
                        {
                            for (var n = j; n < j + 3; n++)
                            {
                                if (sqr[grid[m, n]] && grid[m, n] > 0)
                                {
                                    return false;
                                }
                                sqr[grid[m, n]] = true;
                            }
                        }
                    }
                }
            }
            return true;
        }
    }
}