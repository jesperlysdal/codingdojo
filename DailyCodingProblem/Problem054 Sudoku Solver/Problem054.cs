//Sudoku is a puzzle where you're given a partially-filled 9 by 9 grid with digits.
//The objective is to fill the grid with the constraint that every row, column, and box (3 by 3 subgrid) must contain all of the digits from 1 to 9.
//Implement an efficient sudoku solver.

using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace DailyCodingProblem.Problem054
{
    public class Problem054 : IProblem
    {
        public int Problem { get; } = 54;
        
        public SudokuSolution SolveSudoku(char[,] board)
        {
            if (board == null)
            {
                throw new ArgumentNullException(nameof(board));
            }

            if (board.GetLength(0) < 9 || board.GetLength(1) < 9)
            {
                throw new ArgumentOutOfRangeException(nameof(board), "Board must be 9 by 9.");
            }

            var sw = new Stopwatch();
            sw.Start();
            var result = Solve(board, 0, 0);
            sw.Stop();
            Console.WriteLine("Solving this sudoku took: " + sw.Elapsed);
            return result;
        }

        private SudokuSolution Solve(char[,] board, int row, int col)
        {
            if (row > 8)
            {
                return new SudokuSolution()
                {
                    IsSolved = true,
                    Board = board
                };
            }

            var isEmpty = !char.IsDigit(board[row, col]);

            var nextRow = col == 8 ? (row + 1) : row;
            var nextCol = col == 8 ? 0 : (col + 1);

            if (!isEmpty)
            {
                return Solve(board, nextRow, nextCol);
            }

            var availableNumbers = AvailableNumbers(board, row, col);

            foreach (var option in availableNumbers)
            {
                board[row, col] = option;

                var result = Solve(board, nextRow, nextCol);

                if (result.IsSolved)
                {
                    return result;
                }

                board[row, col] = '\0';
            }

            return new SudokuSolution()
            {
                IsSolved = false,
                Board = board
            };
        }

        private static IEnumerable<char> AvailableNumbers(char[,] board, int currentRow, int currentCol)
        {
            var available = new List<char> {'1', '2', '3', '4', '5', '6', '7', '8', '9'};

            // check rows
            for (var col = 0; col < 9; col++)
            {
                var visit = board[currentRow, col];

                if (char.IsDigit(visit))
                {
                    available.Remove(visit);
                }
            }

            // check cols
            for (var row = 0; row < 9; row++)
            {
                var visit = board[row, currentCol];

                if (char.IsDigit(visit))
                {
                    available.Remove(visit);
                }
            }
            
            // check 3 * 3 matrixes
            var startRow = currentRow / 3 * 3;
            var startCol = currentCol / 3 * 3;
            for (var row = startRow; row < startRow + 3; row++)
            {
                for (var col = startCol; col < startCol + 3; col++)
                {
                    var visit = board[row, col];

                    if (char.IsDigit(visit))
                    {
                        available.Remove(visit);
                    }
                }
            }

            return available;
        }
    }
}