namespace DailyCodingProblem.Problem054
{
    public class SudokuSolution
    {
        public bool IsSolved { get; set; }
        public char[,] Board { get; set; }
    }
}