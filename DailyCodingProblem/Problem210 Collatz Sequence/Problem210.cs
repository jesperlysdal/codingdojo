//A collatz sequence in mathematics can be defined as follows. Starting with any positive integer.
// - If n is even, the  next number in the sequence is n / 2.
// - If n is odd, the next number in the sequence is 3n + 1.
//It is conjectured that every such sequence eventually reaches the number 1. Test this conjecture.
//Bonus: What input n <= 1.000.000 gives the longest sequence?

namespace DailyCodingProblem.Problem210
{
    public class Problem210 : IProblem
    {
        public int Problem { get; } = 210;
        private int _iterations;

        public (int,int) CollatzSequence(int n, bool firstIteration = false)
        {
            if (firstIteration)
                _iterations = 0;
            
            if (n <= 1)
                return (1, _iterations);

            _iterations++;

            return n % 2 == 0 
                ? CollatzSequence(n / 2) 
                : CollatzSequence(3 * n + 1);
        }
    }
}