using NUnit.Framework;

namespace DailyCodingProblem.Problem210
{
    [TestFixture]
    public class Problem210UT
    {
        [Test]
        public void test()
        {
            var sut = new Problem210();

            var longestSequence = 0;
            
            for (var i = 0; i < 1000000; i++)
            {
                var (result, iterations) = sut.CollatzSequence(i, true);

                if (iterations > longestSequence)
                    longestSequence = iterations;
            
                Assert.AreEqual(1, result);
            }
            
            Assert.AreEqual(475, longestSequence);
            
        }
    }
}