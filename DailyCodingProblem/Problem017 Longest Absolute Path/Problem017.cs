//Suppose we represent our file system by a string in the following manner:
//The string "dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext" represents:
//dir
//    subdir1
//    subdir2
//        file.ext
//The directory dir contains an empty sub-directory subdir1 and a sub-directory subdir2 containing a file file.ext.

//The string "dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext" represents:
//dir
//    subdir1
//        file1.ext
//        subsubdir1
//    subdir2
//        subsubdir2
//            file2.ext
//The directory dir contains two sub-directories subdir1 and subdir2.
//subdir1 contains a file file1.ext and an empty second-level sub-directory subsubdir1.
//subdir2 contains a second-level sub-directory subsubdir2 containing a file file2.ext.

//We are interested in finding the longest (number of characters) absolute path to a file within our file system.
//For example, in the second example above, the longest absolute path is "dir/subdir2/subsubdir2/file2.ext",
//and its length is 32 (not including the double quotes).
//Given a string representing the file system in the above format,
//return the length of the longest absolute path to a file in the abstracted file system.
//If there is no file in the system, return 0.

//Note:
//The name of a file contains at least a period and an extension.
//The name of a directory or sub-directory will not contain a period.

// dir
// \tsubdir1
// \t\tfile1.ext
// \t\tsubsubdir1
// \tsubdir2
// \t\tsubsubdir2
// \t\t\tfile2.ext


using System.Collections.Generic;
using System.Linq;

namespace DailyCodingProblem.Problem017
{
    public class Problem017 : IProblem
    {
        public int Problem { get; } = 17;
        
        public int LongestAbsolutePath(string directoryRepresentation)
        {
            if (!directoryRepresentation.Contains("."))
                return 0;

            var absolutePaths = new List<string>();
            
            var paths = directoryRepresentation
                .Split('\n')
                .ToList();

            for (var i = 0; i < paths.Count; i++)
            {
                if (paths[i].Contains("."))
                {
                    var tabs = paths[i].Count(e => e.ToString() == "\t");

                    var pathArray = new string[tabs + 1];
                    pathArray[tabs] = paths[i].Replace("\t", string.Empty);

                    for (var j = 0; j <= i; j++)
                    {
                        var currentTabs = paths[i - j].Count(e => e.ToString() == "\t");
                        if (currentTabs < tabs)
                        {
                            tabs = currentTabs;
                            pathArray[tabs] = paths[i - j].Replace("\t", string.Empty);
                        }
                    }
                    absolutePaths.Add(string.Join("/", pathArray));
                }
            }

            return absolutePaths
                .Select(e => e.Length)
                .Max();
        }
    }
}