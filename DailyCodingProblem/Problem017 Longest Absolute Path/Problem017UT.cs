using NUnit.Framework;

namespace DailyCodingProblem.Problem017
{
    [TestFixture]
    public class Problem17UT
    {
        [Test]
        [TestCase("dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext", 32)]
        public void find_longest_absolute_path(string directoryRepresentation, int expected)
        {
            var sut = new Problem017();
            var result = sut.LongestAbsolutePath(directoryRepresentation);

            Assert.AreEqual(expected, result);
        }
    }
}