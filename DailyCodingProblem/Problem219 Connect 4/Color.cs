﻿namespace DailyCodingProblem.Problem219_Connect_4
{
    public enum Color
    {
        None = 0,
        Red = 1,
        Blue = 2
    }
}