﻿namespace DailyCodingProblem.Problem219_Connect_4
{
    public class GameModel
    {
        public GameModel(
            Color playerTurn, 
            Color[][] grid,
            Color? winner = null)
        {
            PlayerTurn = playerTurn;
            Grid = grid;
            if (winner.HasValue)
                Winner = winner.Value;
        }
        
        public Color PlayerTurn { get; set; }
        public Color? Winner { get; set; }
        public Color[][] Grid { get; set; }
    }
}