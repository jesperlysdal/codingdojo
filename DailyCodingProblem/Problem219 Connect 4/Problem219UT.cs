﻿using NUnit.Framework;

namespace DailyCodingProblem.Problem219_Connect_4
{
    [TestFixture]
    public class Problem219UT
    {
        [Test]
        public void test_new_game()
        {
            var sut = new Problem219();

            var result = sut.NewGame();
            
            var expectedGrid = new []
            {
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None}
            };
            
            Assert.AreEqual(expectedGrid, result.Grid);
            Assert.AreEqual(Color.Red, result.PlayerTurn);
        }

        [Test]
        public void test_choose_and_reset_game()
        {
            var sut = new Problem219();
            
            var result = sut.Choose(5);
            
            var expectedGrid = new []
            {
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None}
            };
            
            Assert.AreEqual(expectedGrid, result.Grid);
            Assert.AreEqual(Color.Blue, result.PlayerTurn);

            result = sut.Choose(4);
            
            expectedGrid = new []
            {
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.Blue,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None}
            };
            
            Assert.AreEqual(expectedGrid, result.Grid);

            result = sut.Choose(4);
            
            expectedGrid = new []
            {
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.Blue,Color.Red,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None}
            };
            
            Assert.AreEqual(expectedGrid, result.Grid);

            result = sut.NewGame();
            
            expectedGrid = new []
            {
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None}
            };
            
            Assert.AreEqual(expectedGrid, result.Grid);
            Assert.AreEqual(Color.Red, result.PlayerTurn);
        }

        [Test]
        public void test_vertical_connect_four_check()
        {
            var sut = new Problem219();
            
            sut.SetGrid(new []
            {
                new [] {Color.Red,Color.Red,Color.Red,Color.Red,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None}
            });

            var result = sut.CheckForConnectedFour();
            
            Assert.AreEqual(true, result);

        }

        [Test]
        public void test_horizontal_connected_four()
        {
            var sut = new Problem219();
            
            sut.SetGrid(new []
            {
                new [] {Color.Red,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None}
            });

            var result = sut.CheckForConnectedFour();
            
            Assert.AreEqual(true, result);
        }

        [Test]
        public void test_diagonal_connected_four_one_direction()
        {
            var sut = new Problem219();
            
            sut.SetGrid(new []
            {
                new [] {Color.Red,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.Red,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.Red,Color.None,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.None,Color.Red,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None}
            });

            var result = sut.CheckForConnectedFour();
            
            Assert.AreEqual(true, result);
            
            sut.SetGrid(new []
            {
                new [] {Color.Red,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.Red,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.Red,Color.Red,Color.None,Color.None},
                new [] {Color.None,Color.Red,Color.None,Color.Red,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.Red,Color.None,Color.Red,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.Red}
            });

            result = sut.CheckForConnectedFour();
            
            Assert.AreEqual(true, result);
            
            sut.SetGrid(new []
            {
                new [] {Color.Red,Color.None,Color.Red,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.Red,Color.None,Color.Red,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.None,Color.None,Color.Red,Color.None},
                new [] {Color.Red,Color.None,Color.Red,Color.Red,Color.None,Color.Red},
                new [] {Color.None,Color.Red,Color.None,Color.Red,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.Red,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.Red}
            });

            result = sut.CheckForConnectedFour();
            
            Assert.AreEqual(true, result);
        }

        [Test]
        public void test_diagonal_connected_four_other_direction()
        {
            var sut = new Problem219();
            
            sut.SetGrid(new []
            {
                new [] {Color.Red,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.None,Color.Red,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.Red,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.Red,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None}
            });
            
            var result = sut.CheckForConnectedFour();
            
            Assert.AreEqual(true, result);
        }

        [Test]
        public void test_win_condition()
        {
            var sut = new Problem219();
            
            sut.SetGrid(new []
            {
                new [] {Color.Red,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.None,Color.Red,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.Red,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.Red,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.Red,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None}
            });
            sut.SetPlayerTurn(Color.Red);
            var result = sut.Choose(2);
            
            Assert.AreEqual(true, result.Winner.HasValue);
        }
    }
}