﻿//Connect 4 is a game where opponents take turns dropping red or blue discs into a 7 x 6 vertically suspended grid.
//The game ends when one player creates a line of four consecutive discs of their color (horizontally, vertically or diagonally),
//or when there are no more spots left in the grid.

//Design and implement Connect 4.

using System;
using System.Collections.Generic;
using System.Linq;

namespace DailyCodingProblem.Problem219_Connect_4
{
    public class Problem219
    {
        private Color[][] _grid;
        private Color _playerTurn;
        
        private readonly int[] _allowedInputs = {1, 2, 3, 4, 5, 6, 7};

        private readonly (int, int)[][] _diagonalChecks = 
        {
            new[] {(1,3), (2,4), (3,5), (4,6)},
            new[] {(1,2), (2,3), (3,4), (4,5), (5,6)},
            new[] {(1,1), (2,2), (3,3), (4,4), (5,5), (6,6)},
            new[] {(2,1), (3,2), (4,3), (5,4), (6,5), (7,6)},
            new[] {(3,1), (4,2), (5,3), (6,4), (7,5)},
            new[] {(4,1), (5,2), (6,3), (7,4)},
            
            new[] {(7,3), (6,4), (5,5), (4,6)},
            new[] {(7,2), (6,3), (5,4), (4,5), (3,6)},
            new[] {(7,1), (6,2), (5,3), (4,4), (3,5), (2,6)},
            new[] {(6,1), (5,2), (4,3), (3,4), (2,5), (1,6)},
            new[] {(5,1), (4,2), (3,3), (2,4), (1,5)},
            new[] {(4,1), (3,2), (2,3), (1,4)}
        };

        public Problem219()
        {
            Initialize();
        }
        
        public void ConnectFourGameLoop()
        {
            NewGame();
            DrawGrid();
            Console.WriteLine("Press any button to start!");
            var input = Console.ReadLine();
            while (true)
            {
                int number = 0;
                while (input == null || !int.TryParse(input, out number))
                {
                    if (_allowedInputs.Contains(number) && !ColumnIsFull(number))
                        break;
                    
                    Console.WriteLine($"{_playerTurn} player pick a row 1-7:");
                    input = Console.ReadLine();
                }
                
                if (input == "Q")
                    break;

                var result = Choose(number - 1);
                Console.Clear();
                DrawGrid();
                input = string.Empty;

                if (result.Winner.HasValue)
                {
                    Console.WriteLine($"{result.Winner.Value} player wins!");
                    Console.WriteLine("Starting new game, enter \"Q\" to quit.");
                    NewGame();
                    DrawGrid();
                }
            }
        }

        public void SetGrid(Color[][] grid)
        {
            _grid = grid;
        }

        public void SetPlayerTurn(Color color)
        {
            _playerTurn = color;
        }

        private void Initialize()
        {
            _playerTurn = Color.Red;
            _grid = new []
            {
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None},
                new [] {Color.None,Color.None,Color.None,Color.None,Color.None,Color.None}
            };
        }

        public GameModel NewGame()
        {
            Initialize();
            return new GameModel(_playerTurn, _grid);
        }

        public GameModel Choose(int row)
        {
            if (ColumnIsFull(row))
                throw new Exception("Column is full, pick another column!");

            if (GridIsFull())
                return new GameModel(
                    playerTurn: _playerTurn, 
                    grid: _grid, 
                    winner: _playerTurn == Color.Red 
                        ? Color.Blue 
                        : Color.Red);
            
            var firstEmptyIndex = Array.LastIndexOf(_grid[row], _grid[row].Last(e => e == (int)Color.None));
            _grid[row][firstEmptyIndex] = _playerTurn;
            
            var model = new GameModel(_playerTurn, _grid);
            
            if (CheckForConnectedFour())
            {
                model.Winner = _playerTurn;
                return model;
            }
            
            _playerTurn = _playerTurn == Color.Red 
                ? Color.Blue 
                : Color.Red;

            model = new GameModel(_playerTurn, _grid);

            return model;
        }

        public void DrawGrid()
        {
            var rowLength = _grid.Length;
            var colLength = _grid[0].Length;

            for (var i = 0; i < colLength; i++)
            {
                for (var j = 0; j < rowLength; j++)
                {
                    var color = _grid[j][i];
                    
                    Console.ForegroundColor = color == Color.None
                        ? ConsoleColor.White
                        : color == Color.Blue
                            ? ConsoleColor.Blue
                            : ConsoleColor.Red;

                    var symbol = color == Color.None
                        ? "O"
                        : color == Color.Blue
                            ? "B"
                            : "R";
                    Console.Write($"{symbol}    ");
                }
                Console.Write(Environment.NewLine + Environment.NewLine);
            }

            Console.ForegroundColor = ConsoleColor.Green;
            for (var i = 0; i < 7; i++)
            {
                Console.Write($"{i + 1}    ");
            }
            Console.WriteLine(Environment.NewLine);
        }

        public bool ColumnIsFull(int rowDrop)
        {
            return _grid[rowDrop].All(e => e != (int)Color.None);
        }

        private bool GridIsFull()
        {
            return _grid.Select(row => row.All(e => e != (int) Color.None))
                .All(e => e);
        }

        internal bool CheckForConnectedFour()
        {
            return CheckVertical() || CheckHorizontal() || CheckDiagonal();
        }

        private bool CheckDiagonal()
        {
            var diagonalCounter = 0;
            foreach (var coordinates in _diagonalChecks)
            {
                foreach (var coordinate in coordinates)
                {
                    if (_grid[coordinate.Item1 - 1][coordinate.Item2 - 1] == _playerTurn)
                    {
                        diagonalCounter++;
                    }
                    else
                    {
                        diagonalCounter = 0;
                    }

                    if (diagonalCounter == 4)
                        return true;
                }
            }

            return false;
        }

        private bool CheckVertical()
        {
            var verticalCounter = 0;
            for (int i = 0; i < 7; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    if (_grid[i][j] == _playerTurn)
                    {
                        verticalCounter++;
                    }
                    else
                    {
                        verticalCounter = 0;
                    }

                    if (verticalCounter == 4)
                        return true;
                }
            }

            return false;
        }

        private bool CheckHorizontal()
        {
            var horizontalCounter = 0;
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    if (_grid[j][i] == _playerTurn)
                    {
                        horizontalCounter++;
                    }
                    else
                    {
                        horizontalCounter = 0;
                    }

                    if (horizontalCounter == 4)
                        return true;
                }
            }

            return false;
        }
    }
}