using System;
using System.Diagnostics;
using System.Linq;
using NUnit.Framework;

namespace DailyCodingProblem.Problem069
{
    [TestFixture]
    public class Problem069UT
    {
        [Test]
        [TestCase(new[]{-10, -10, 5, 2}, 5)]
        public void test_find_largest_integer(int[] input, int expected)
        {
            var sut = new Problem069();
            var result = sut.LargestPositives(1, input.ToList())[0];
            
            Assert.AreEqual(expected, result);
        }
        
        [Test]
        [TestCase(new[]{-9, -10, -10, -9, 5, 2}, -10)]
        public void test_find_lowest_integer(int[] input, int expected)
        {
            var sut = new Problem069();
            var result = sut.LargestNegatives(1, input.ToList())[0];
            
            Assert.AreEqual(expected, result);
        }
        
        [Test]
        [TestCase(new[]{10, 10, 5, 2}, 500)]
        [TestCase(new[]{-10, -10, 5, 2}, 500)]
        [TestCase(new[]{-10, -10, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 5, 2, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2}, 500)]
        public void test_find_largest_product(int[] input, int expected)
        {
            var sut = new Problem069();
            var sw = new Stopwatch();
            sw.Start();
            var result = sut.LargestProductOfThreeNumbers(input);
            sw.Stop();

            var elapsed = sw.Elapsed;
            
//            Assert.AreEqual(expected: expected, result);
        }
    }
}