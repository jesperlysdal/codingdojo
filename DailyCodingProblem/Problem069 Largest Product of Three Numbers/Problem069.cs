//Given a list of integers, return the largest product that can be made by multiplying any three integers.
//For example, if the list is [-10, -10, 5, 2], we should return 500, since that's -10 * -10 * 5.
//You can assume the list has at least three integers.

using System.Collections.Generic;
using System.Linq;

namespace DailyCodingProblem.Problem069
{
    public class Problem069 : IProblem
    {
        public int Problem { get; } = 69;
        
        public int LargestProductOfThreeNumbers(int[] input)
        {
            if (input.Length < 3)
                return 0;
            
            if (input.Length == 3)
                return input[0] * input[1] * input[2];

            int a, b, c;
            var negatives = new List<int>();
            var positives = new List<int>();

            foreach (var number in input)
            {
                if (number < 0)
                    negatives.Add(number);
                else if (number > 0)
                    positives.Add(number);
            }

            var result = 0;
            
            if (negatives.Count > 1 && positives.Count > 0)
            {
                var largestNegatives = LargestNegatives(2, negatives);
                a = largestNegatives[0];
                b = largestNegatives[1];
                c = LargestPositives(1, positives)[0];
                
                if (a * b * c > result)
                    result = a * b * c;
            }
            else if (positives.Count > 2)
            {
                var largestPositives = LargestPositives(3, positives);
                a = largestPositives[0];
                b = largestPositives[1];
                c = largestPositives[2];
                if (a * b * c > result)
                    result = a * b * c;
            }

            return result;
        }

        public int[] LargestNegatives(int n, List<int> negatives)
        {
            if (n == negatives.Count)
                return negatives.ToArray();
            
            return negatives
                .OrderBy(e => e)
                .Take(n)
                .ToArray();
        }

        public int[] LargestPositives(int n, List<int> positives)
        {
            if (n == positives.Count)
                return positives.ToArray();
            
            return positives
                .OrderByDescending(e => e)
                .Take(n)
                .ToArray();
        }
    }
}