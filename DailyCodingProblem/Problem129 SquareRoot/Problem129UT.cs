﻿using NUnit.Framework;

namespace DailyCodingProblem.Problem129
{
    [TestFixture]
    public class Problem129UT
    {
        [Test]
        [TestCase(9, 3)]
        public void test(int input, int expected)
        {
            var sut = new Problem129();

            var result = sut.SquareRoot(input);
            
            Assert.AreEqual(expected, result);
        }
    }
}