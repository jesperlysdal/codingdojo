﻿//Given a real number n, find the square root of n. For example, given n = 9, return 3.

namespace DailyCodingProblem.Problem129
{
    public class Problem129
    {
        public int SquareRoot(int number)
        {
            for (var i = 0; i < 10000; i++)
            {
                if (i * i == number)
                    return i;
            }

            return 0;
        }
    }
}