﻿//You're given a string consisting solely of ( , ) and *. * can represent either a ( , ) or an empty string.
//Determine whether the parentheses are balanced.

//For example, (()* and (*) are balanced. )*( is not balanced.

using System;

namespace DailyCodingProblem.Problem142
{
    public class Problem142
    {
        public bool IsBalanced(string input)
        {
            if (input[0] == ')' || input[input.Length - 1] == '(')
                return false;

            var starts = 0;
            var ends = 0;
            var wildcards = 0;

            foreach (var symbol in input)
            {
                switch (symbol)
                {
                    case '(':
                        starts++;
                        break;
                    case ')':
                        ends++;
                        break;
                    case '*':
                        wildcards++;
                        break;
                    default:
                        throw new ArgumentException($"Unrecognised character '{symbol}'. Input can only consist of '(', ')' and '*'.");
                }
            }

            return 
                starts == ends || 
                starts < ends 
                    ? starts + wildcards >= ends 
                    : ends + wildcards >= starts;
        }
    }
}