﻿//For example, (()* and (*) are balanced. )*( is not balanced.

using NUnit.Framework;

namespace DailyCodingProblem.Problem142
{
    [TestFixture]
    public class Problem142UT
    {
        [Test]
        [TestCase("(()*", true)]
        [TestCase("(*)", true)]
        [TestCase(")*(", false)]
        public void test(string input, bool expected)
        {
            var sut = new Problem142();

            var result = sut.IsBalanced(input);
            
            Assert.AreEqual(expected, result);
        }
    }
}