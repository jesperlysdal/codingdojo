using NUnit.Framework;

namespace DailyCodingProblem.Problem184
{
    [TestFixture]
    public class Problem184UT
    {
        [Test]
        [TestCase(new[] {42, 56, 14}, 14)]
        [TestCase(new[] {6, 9, 12}, 3)]
        [TestCase(new[] {10, 14, 15}, 1)]
        [TestCase(new[] {15, 20, 25}, 5)]
        public void test(int[] input, int expected)
        {
            var result = Problem184.GreatestCommonDenominator(input);
            
            Assert.AreEqual(expected, result);
        }
    }
}