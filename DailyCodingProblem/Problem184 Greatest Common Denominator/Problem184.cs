//Given n numbers, find the greatest common denominator between them.
//For example, given the numbers [42, 56, 14], return 14.

using System.Linq;

namespace DailyCodingProblem.Problem184
{
    public class Problem184 : IProblem
    {
        public int Problem { get; } = 184;

        public static int GreatestCommonDenominator(int[] input)
        {
            var min = input.Min();

            while (true)
            {
                if (input.All(number => number % min == 0))
                    return min;

                min -= 1; //TODO this can be optimized
            }
        }
    }
}