//Given a string and a pattern, find the starting indices of all occurrences of the pattern in the string.
//For example, given the string "abracadabra" and the pattern "abr", you should return [0, 7].

using System.Collections.Generic;

namespace DailyCodingProblem.Problem211
{
    public class Problem211
    {
        public int[] IndexesOfOccurances(string input, string pattern)
        {
            var result = new List<int>();
            
            for (var i = 0; i < input.Length; i++)
            {
                if (input[i] != pattern[0]) continue;
                
                var match = true;
                for (var j = 1; j < pattern.Length; j++)
                {
                    if (i + j < input.Length && input[i + j] == pattern[j]) 
                        continue;
                    
                    match = false;
                    break;
                }
                
                if (match)
                    result.Add(i);
            }

            return result.ToArray();
        }
    }
}