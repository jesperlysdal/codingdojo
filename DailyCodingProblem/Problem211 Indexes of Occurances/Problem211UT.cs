using NUnit.Framework;

namespace DailyCodingProblem.Problem211
{
    [TestFixture]
    public class Problem211UT
    {
        [Test]
        public void test()
        {
            var sut = new Problem211();

            var result = sut.IndexesOfOccurances("abracadabra", "abr");
            
            Assert.AreEqual(new[]{0,7}, result);
        }
    }
}