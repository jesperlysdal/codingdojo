//Alice wants to join her school's Probability Student Club.
//Membership dues are computed via one of two simple probabilistic games.
//The first game: roll a die repeatedly. Stop rolling once you get a five followed by a six. Your number of rolls is the amount you pay, in dollars.
//The second game: same, except that the stopping condition is a five followed by a five.
//Which of the two games should Alice elect to play? Does it even matter?
//Write a program to simulate the two games and calculate their expected value.

using System;

namespace DailyCodingProblem.Problem178
{
    public class Problem178 : IProblem
    {
        public int Problem { get; } = 178;

        private readonly Random _random;
        private int _rolls;

        public Problem178()
        {
            _random = new Random();
        }

        public int SimulateGameOfDices(int firstValue, int followedByValue, int iterations = 10000)
        {
            var numberOfRolls = 0;
            for (var i = 0; i < iterations; i++)
            {
                numberOfRolls += GameOfDices(firstValue, followedByValue);
            }

            return numberOfRolls / iterations;
        }

        private int GameOfDices(int firstValue, int followedByValue)
        {
            if (firstValue < 1 || firstValue > 6)
                throw new ArgumentException();
            
            if (followedByValue < 1 || followedByValue > 6)
                throw new ArgumentException();
            
            _rolls = 0;
            
            while (true)
            {
                if (!RollFor(firstValue)) 
                    continue;
                
                if (!RollFor(followedByValue)) 
                    continue;
                
                return _rolls;
            }
        }

        private bool RollFor(int desiredValue)
        {
            return Roll() == desiredValue;
        }

        private int Roll()
        {
            _rolls += 1;
            return _random.Next(1, 7);
        }
    }
}