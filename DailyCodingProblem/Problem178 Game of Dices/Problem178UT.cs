using NUnit.Framework;

namespace DailyCodingProblem.Problem178
{
    [TestFixture]
    public class Problem178UT
    {
        [Test]
        public void test()
        {
            var sut = new Problem178();

            var averageNumberOfRolls1 = sut.SimulateGameOfDices(5, 6, 20000);
            var averageNumberOfRolls2 = sut.SimulateGameOfDices(5, 5, 20000);

            Assert.AreEqual(averageNumberOfRolls1, averageNumberOfRolls2, 2);
        }
    }
}