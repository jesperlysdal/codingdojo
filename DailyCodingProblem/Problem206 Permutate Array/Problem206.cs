﻿//A permutation can be specified by an array P, where P[i] represents the location of the element at i in the permutation.
//For example, [2,1,0] represents the permutation where elements at the index 0 and 2 are swapped.

//Given an array and a permutation, apply the permutation to the array. For example, given the array ["a", "b", "c"] and the 
//permutation [2,1,0], return ["c", "b", "a"].

using System.Linq;
using System.Text;

namespace DailyCodingProblem.Problem206
{
    public class Problem206
    {
        public string[] Permutate(string[] input, int[] permutation)
        {
            var result = new string[input.Length];
            for (var i = 0; i < input.Length; i++)
            {
                result[i] = input[permutation[i]];
            }

            return result;
        }
        
        //Bonus: do the same for a string input

        public string Permutate(string input, int[] permutation)
        {
            var result = new string[input.Length];
            for (var i = 0; i < input.Length; i++)
            {
                result[i] = input[permutation[i]].ToString();
            }
            
            var sb = new StringBuilder();

            foreach (var letter in result)
            {
                sb.Append(letter);
            }

            return sb.ToString();
        }
        
    }
}