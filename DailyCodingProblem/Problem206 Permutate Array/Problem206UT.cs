﻿//Given an array and a permutation, apply the permutation to the array. For example, given the array ["a", "b", "c"] and the 
//permutation [2,1,0], return ["c", "b", "a"].
using NUnit.Framework;

namespace DailyCodingProblem.Problem206
{
    [TestFixture]
    public class Problem206UT
    {
        [Test]
        [TestCase(new[] {"a", "b", "c"}, new[] {2, 1, 0}, new[]{"c", "b", "a"})]
        [TestCase(new[] {"a", "b", "c"}, new[] {1, 1, 1}, new[]{"b", "b", "b"})]
        [TestCase(new[] {"a", "b", "c"}, new[] {2, 0, 1}, new[]{"c", "a", "b"})]
        public void test(string[] input, int[] permutation, string[] expected)
        {
            var sut = new Problem206();

            var result = sut.Permutate(input, permutation);
            
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void test_bonus()
        {
            var sut = new Problem206();

            var result = sut.Permutate("abc", new[] {2, 1, 0});
            
            Assert.AreEqual("cba", result);
        }
    }
}