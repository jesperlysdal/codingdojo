// There are N prisoners standing in a circle, waiting to be executed.
// The executions are carried out starting with the kth person, and removing every successive kth person going clockwise until there is no one left.
// Given N and k, write an algorithm to determine where a prisoner should stand in order to be the last survivor.
// For example, if N = 5 and k = 2, the order of executions would be [2, 4, 1, 5, 3], so you should return 3.
// Bonus: Find an O(log N) solution if k = 2.

using System.Collections.Generic;
using System.Linq;

namespace DailyCodingProblem.Problem225_Prisoner_Execution
{
    public class Problem225
    {
        public int OptimalIndex(int numberOfPrisoners, int executeIndex)
        {
            var executionOrder = new int[numberOfPrisoners];
            var currentIndex = 0;

            for (var i = 0; i < numberOfPrisoners; i++)
            {
                currentIndex += executeIndex;
                if (currentIndex > numberOfPrisoners)
                {
                    currentIndex -= numberOfPrisoners;
                }

                executionOrder[i] = currentIndex;
            }

            var test = 0;


            return 3;
        }
    }
}