using NUnit.Framework;

namespace DailyCodingProblem.Problem225_Prisoner_Execution
{
    [TestFixture]
    public class Problem225UT
    {
        [Test]
        public void test()
        {
            var sut = new Problem225();

            var result = sut.OptimalIndex(5, 2);
            
            Assert.AreEqual(3, result);
        }
    }
}