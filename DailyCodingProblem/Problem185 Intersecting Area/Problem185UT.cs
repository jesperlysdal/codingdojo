using NUnit.Framework;

namespace DailyCodingProblem.Problem185
{
    [TestFixture]
    public class Problem185UT
    {
        [Test]
        public void test()
        {
            var result = Problem185.IntersectingArea(
                new Rectangle((1, 4), (3, 3)), 
                new Rectangle((0, 5), (4, 3)));
            var expected = 6;
            
            Assert.AreEqual(expected, result);
            
//            result = Problem185.IntersectingArea(
//                new Rectangle((1, 4), (3, 3)), 
//                new Rectangle((2, 3), (4, 3)));
//            expected = 0;
//            
//            Assert.AreEqual(expected, result);
        }
    }
}