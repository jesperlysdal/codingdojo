//Given two rectangles on a 2D graph, return the area of their intersection. If the rectangles don't intersect, return 0.
//For example, given the following rectangles:
//{
//"top_left": (1, 4),
//"dimensions": (3, 3) # width, height
//}
//and
//{
//"top_left": (0, 5),
//"dimensions": (4, 3) # width, height
//}
//return 6.

//TODO cannot yet handle rectangles in negative space
//possible solution is to move everything into positive space, keeping the relative positioning

using System;
using System.Linq;

namespace DailyCodingProblem.Problem185
{
    public class Problem185 : IProblem
    {
        public int Problem { get; } = 185;

        public static int IntersectingArea(Rectangle first, Rectangle second)
        {
            var maxHeight = Math.Max(first.TopLeft.Item2, second.TopLeft.Item2);
            var maxWidth = Math.Max(
                first.TopLeft.Item1 + first.Dimensions.Item1,
                second.TopLeft.Item1 + second.Dimensions.Item1);

            var grid = new int[maxWidth, maxHeight];
            PlotRectangle(first, grid);
            PlotRectangle(second, grid);

            return grid.Cast<int>().Count(field => field == 2);
        }

        private static void PlotRectangle(Rectangle rectangle, int[,] grid)
        {
            for (var i = rectangle.TopLeft.Item1 + rectangle.Dimensions.Item2; i > rectangle.TopLeft.Item1; i--)
            {
                for (var j = rectangle.TopLeft.Item1 + rectangle.Dimensions.Item1; j > rectangle.TopLeft.Item1; j--)
                {
                    grid[i - 1, j - 1] += 1;
                }
            }
        }
    }
}