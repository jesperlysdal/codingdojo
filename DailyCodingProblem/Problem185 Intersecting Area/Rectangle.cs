namespace DailyCodingProblem.Problem185
{
    public class Rectangle
    {
        public Rectangle((int,int) topLeft, (int,int) dimensions)
        {
            TopLeft = topLeft;
            Dimensions = dimensions;
        }
        
        public (int, int) TopLeft { get; set; }
        public (int, int) Dimensions { get; set; }
    }
}