using NUnit.Framework;

namespace DailyCodingProblem.Problem029
{
    [TestFixture]
    public class Problem029UT
    {
        [Test]
        [TestCase("A", "1A")]
        [TestCase("AA", "2A")]
        [TestCase("AAABBCDDDD", "3A2B1C4D")]
        [TestCase("1234", "")]
        public void encode(string input, string expected)
        {
            var sut = new Problem029();
            var result = sut.Encode(input);
            
            Assert.AreEqual(expected: expected, actual: result);
        }

        [Test]
        [TestCase("3A2B1C4D", "AAABBCDDDD")]
        [TestCase("A", null)]
        [TestCase("1", null)]
        [TestCase("1A", "A")]
        [TestCase("0A", "")]
        [TestCase("0A2B", "BB")]
        [TestCase("A2A", null)]
        public void decode(string input, string expected)
        {
            var sut = new Problem029();
            var result = sut.Decode(input);
            
            Assert.AreEqual(expected: expected, actual: result);
        }
    }
}