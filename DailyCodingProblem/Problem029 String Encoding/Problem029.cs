//Run-length encoding is a fast and simple method of encoding strings.
//The basic idea is to represent repeated successive characters as a single count and character.
//For example, the string "AAAABBBCCDAA" would be encoded as "4A3B2C1D2A".
//Implement run-length encoding and decoding.
//You can assume the string to be encoded have no digits and consists solely of alphabetic characters.
//You can assume the string to be decoded is valid.

using System.Linq;

namespace DailyCodingProblem.Problem029
{
    public class Problem029 : IProblem
    {
        public int Problem { get; } = 29;
        
        public string Encode(string input)
        {
            var result = string.Empty;
            
            if (input.Any(char.IsDigit))
                return result;
            
            if (input.Length == 1)
                return $"1{input}";
            
            var maxIndex = input.Length - 1;
            for (var i = 0; i < maxIndex; i += 0)
            {
                if (i >= maxIndex)
                    return result;
                
                var j = i + 1;
                while (input[j] == input[i] && j < maxIndex)
                {
                    j++;
                }

                var numberOfLetters = j == maxIndex 
                    ? j - i + 1
                    : j - i;
                
                result += $"{numberOfLetters}{input[i]}";
                i += numberOfLetters;
            }
            
            return result;
        }

        public string Decode(string input)
        {
            if (input.Length < 2)
                return null;
            
            var result = string.Empty;

            for (var i = 0; i < input.Length; i += 2)
            {
                if (!char.IsDigit(input[i]))
                    return null;
                
                if (i + 1 < input.Length)
                    result += string.Concat(Enumerable.Repeat(input[i + 1], int.Parse(input[i].ToString())));
            }
            
            return result;
        }
    }
}