//Given an array of time intervals (start, end) for classroom lectures (possibly overlapping),
//find the minimum number of rooms required.
//For example, given [(30, 75), (0, 50), (60, 150)], you should return 2.

using System.Collections.Generic;
using System.Linq;

namespace DailyCodingProblem.Problem021
{
    public class Problem021 : IProblem
    {
        public int Problem { get; } = 21;

        public int ClassRoomsNeeded((int, int)[] lectures)
        {
            var overlappingLecturesIndexes = new List<int>();
            var overlaps = 0;

            for (var i = 0; i < lectures.Length; i++)
            {
                if (overlappingLecturesIndexes.Contains(i))
                    continue;

                for (var j = 0; j < lectures.Length; j++)
                {
                    if (j != i && (lectures[i].Item1 < lectures[j].Item2 || lectures[i].Item2 > lectures[j].Item1))
                    {
                        overlappingLecturesIndexes.Add(i);
                        overlappingLecturesIndexes.Add(j);
                        overlaps++;
                    }
                }
            }

            return overlaps;
        }
    }
}