//For example, given [(30, 75), (0, 50), (60, 150)], you should return 2.

using NUnit.Framework;

namespace DailyCodingProblem.Problem021
{
    [TestFixture]
    public class Problem21UT
    {
        [Test]
        public void test()
        {
            var sut = new Problem021();
            var lectures = new[] {(30, 75), (0, 50), (60, 150)};
            var expected = 2;

            var result = sut.ClassRoomsNeeded(lectures);
            
            Assert.AreEqual(expected, result);
        }
    }
}