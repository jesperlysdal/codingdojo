namespace DailyCodingProblem.Problem138_Coins_required_to_make_n_cents
{
    public class Problem138
    {
        private readonly int[] _coinValues = {25, 10, 5, 1};

        public int MinimumNumberOfCoinsTo(int input)
        {
            if (input <= 0)
                return 0;
            
            var minimumCoins = 0;

            while (input > 0)
            {
                if (input >= _coinValues[0])
                {
                    input -= _coinValues[0];
                    minimumCoins++;
                }
                else if (input >= _coinValues[1])
                {
                    input -= _coinValues[1];
                    minimumCoins++;
                }
                else if (input >= _coinValues[2])
                {
                    input -= _coinValues[2];
                    minimumCoins++;
                }
                else 
                {
                    input -= _coinValues[3];
                    minimumCoins++;
                }
            }

            return minimumCoins;
        }
    }
}