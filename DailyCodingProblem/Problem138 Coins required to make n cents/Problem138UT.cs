using NUnit.Framework;

namespace DailyCodingProblem.Problem138_Coins_required_to_make_n_cents
{
    [TestFixture]
    public class Problem138UT
    {
        [Test]
        public void test()
        {
            var sut = new Problem138();

            var result = sut.MinimumNumberOfCoinsTo(16);
            
            Assert.AreEqual(3, result);
        }
    }
}