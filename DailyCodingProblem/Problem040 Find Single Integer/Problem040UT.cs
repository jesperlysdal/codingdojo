using NUnit.Framework;

namespace DailyCodingProblem.Problem040
{
    [TestFixture]
    public class Problem040UT
    {
        [Test]
        [TestCase(new[] { 6, 1, 3, 3, 3, 6, 6 }, 1)]
        [TestCase(new[] { 13, 19, 13, 13 }, 19)]
        [TestCase(new[] { 13, 19, 13, 13, 19 }, 0)]
        public void find_number_with_only_1_occurance(int[] input, int expected)
        {
            var result = Problem040.FindSingleInteger(input);
            
            Assert.AreEqual(expected, result);
        }
    }
}