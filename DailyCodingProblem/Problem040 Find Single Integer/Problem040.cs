//Given an array of integers where every integer occurs three times except for one integer,
//which only occurs once, find and return the non-duplicated integer.
//For example, given [6, 1, 3, 3, 3, 6, 6], return 1. Given [13, 19, 13, 13], return 19.
//Do this in O(N) time and O(1) space.


using System.Collections.Generic;
using System.Linq;

namespace DailyCodingProblem.Problem040
{
    public class Problem040 : IProblem
    {
        public int Problem { get; } = 40;
        
        public static int FindSingleInteger(IEnumerable<int> input)
        {
            var dic = new Dictionary<int, int>();
            foreach (var integer in input)
            {
                if (!dic.ContainsKey(integer))
                {
                    dic[integer] = 1;
                }
                else
                {
                    dic[integer] += 1;
                }
            }

            return dic.ContainsValue(1) 
                ? dic.First(e => e.Value == 1).Key 
                : 0;
        }
    }
}