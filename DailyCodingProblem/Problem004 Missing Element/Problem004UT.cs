//For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should give 3.

using NUnit.Framework;

namespace DailyCodingProblem.Problem004
{
    [TestFixture]
    public class Problem04UT
    {
        [Test]
        [TestCase(new[]{3, 4, -1, 1}, 2)]
        [TestCase(new[]{0, 1, 2, 3}, 4)]
        [TestCase(new[]{0, 1, 2, 2, 3}, 4)]
        public void test_find_missing_integer(int[] input, int expected)
        {
            var result = Problem004.FindMissingPositiveSecondTry(input);
            
            Assert.AreEqual(expected, result);
        }
    }
}