//Given an array of integers, find the first missing positive integer in linear time and constant space.
//In other words, find the lowest positive integer that does not exist in the array.
//The array can contain duplicates and negative numbers as well.
//For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should give 3.
//You can modify the input array in-place.

using System.Collections.Generic;
using System.Linq;

namespace DailyCodingProblem.Problem004
{
    public class Problem004 : IProblem
    {
        public int Problem { get; } = 4;
        
        public static int FindMissingPositive(int[] input)
        {
            var result = 1;

            var mem = new Dictionary<int, bool>();

            foreach (var number in input)
            {
                if (number > 0 && !mem.ContainsKey(number))
                {
                    mem.Add(number, true);
                }
            }

            while (mem.ContainsKey(result))
            {
                result++;
            }

            return result;
        }

        public static int FindMissingPositiveSecondTry(int[] input)
        {
            var current = 1;
            
            foreach (var number in input.OrderBy(e => e))
            {
                if (number < current)
                    continue;

                if (number > current)
                    return current;
                
                current++;
            }

            return input.Last() + 1;
        }
    }
}