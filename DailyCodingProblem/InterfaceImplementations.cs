using System;
using System.Linq;

namespace DailyCodingProblem
{
    public static class InterfaceImplementations
    {
        public static T[] InstancesOf<T>()
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(assembly => assembly.GetTypes())
                .Where(type => typeof(T).IsAssignableFrom(type))
                .Where(type => !type.IsInterface)
                .Select(type => GetInstance<T>(type))
                .ToArray();
        }
        
        private static T GetInstance<T>(Type type, params object[] parameterList)
        {
            var signature = parameterList
                .Select(p => p.GetType())
                .ToArray();
            
            var constructor = type.GetConstructor(signature);
            var instance = (T) constructor?.Invoke(parameterList);

            return instance;
        }
    }
}