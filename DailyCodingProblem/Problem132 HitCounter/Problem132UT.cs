﻿using System;
using NUnit.Framework;

namespace DailyCodingProblem.Problem132
{
    [TestFixture]
    public class Problem132UT
    {
        [Test]
        public void test()
        {
            var sut = new Problem132();

            sut.Record(new DateTime(2019, 12, 29, 20, 11, 30));
            sut.Record(new DateTime(2019, 12, 29, 21, 11, 30));
            sut.Record(new DateTime(2019, 12, 29, 22, 11, 30));

            var total = sut.Total();
            
            Assert.AreEqual(3, total);

            var range = sut.Range(
                new DateTime(2019, 12, 29, 17, 30, 0),
                new DateTime(2019, 12,29, 21, 15, 0));
            
            Assert.AreEqual(2, range);
        }
    }
}