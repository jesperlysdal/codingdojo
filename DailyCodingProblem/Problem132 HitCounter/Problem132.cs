﻿//Design and implement a HitCounter class that keeps track of requests (or hits). 
//It should support the following operations:
// - record(timestamp): records a hit that happened at timestamp.
// - total(): returns the total number of hits occured.
// - range(lower, upper): returns the number of hits that occured between lower and upper (inclusive).

//Follow-up: What if our system has limited memory?

using System;
using System.Collections.Generic;
using System.Linq;

namespace DailyCodingProblem.Problem132
{
    public class Problem132
    {
        private readonly List<DateTime> _hits;

        public Problem132()
        {
            _hits = new List<DateTime>();
        }
        
        public void Record(DateTime timeStamp)
        {
            _hits.Add(timeStamp);
        }

        public int Total()
        {
            return _hits.Count;
        }

        public int Range(DateTime lower, DateTime upper)
        {
            return _hits.Count(e => e >= lower && e <= upper);
        }
    }
}