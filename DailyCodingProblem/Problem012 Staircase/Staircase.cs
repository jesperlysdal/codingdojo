using System.Collections.Generic;
using System.Linq;

namespace DailyCodingProblem.Problem012
{
    public static class Staircase
    {
        public static int CountUniqueWaysOfClimbing(int stairs, IEnumerable<int> stepSizes)
        {
            if (stairs < 0)
                return 0;
            
            if (stairs == 0)
                return 1;

            var cache = new int[stairs + 1];
            cache[0] = 1;

            for (var i = 1; i < stairs + 1; i++)
            {
                cache[i] = stepSizes
                    .Where(stepSize => i - stepSize >= 0)
                    .Sum(stepSize => cache[i - stepSize]);
            }

            return cache[stairs];
        }
    }
}

//There's a staircase with N steps, and you can climb 1 or 2 steps at a time.
//Given N, write a function that returns the number of unique ways you can climb the staircase.
//The order of the steps matters.

//For example, if N is 4, then there are 5 unique ways:
//1, 1, 1, 1
//2, 1, 1
//1, 2, 1
//1, 1, 2
//2, 2

//What if, instead of being able to climb 1 or 2 steps at a time, you could climb any number from a set of positive integers X?
//For example, if X = {1, 3, 5}, you could climb 1, 3, or 5 steps at a time. Generalize your function to take in X.