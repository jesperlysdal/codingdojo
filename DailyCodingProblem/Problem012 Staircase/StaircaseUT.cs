using NUnit.Framework;

namespace DailyCodingProblem.Problem012
{
    [TestFixture]
    public class StaircaseUT
    {
        [Test]
        public void count_unique_ways_of_climbing_stair1()
        {
            var result = Staircase.CountUniqueWaysOfClimbing(4, new []{1,2});
            
            Assert.AreEqual(5, result);
        }

        [Test]
        public void count_unique_ways_of_climbing_stair2()
        {
            var result = Staircase.CountUniqueWaysOfClimbing(4, new []{1,3,5});
            
            Assert.AreEqual(3, result);
        }
        
        [Test]
        public void count_unique_ways_of_climbing_stair3()
        {
            var result = Staircase.CountUniqueWaysOfClimbing(0, new []{1,2});
            
            Assert.AreEqual(1, result);
        }
        
        [Test]
        public void count_unique_ways_of_climbing_stair4()
        {
            var result = Staircase.CountUniqueWaysOfClimbing(-2, new []{1,2});
            
            Assert.AreEqual(0, result);
        }
    }
}

//There's a staircase with N steps, and you can climb 1 or 2 steps at a time.
//Given N, write a function that returns the number of unique ways you can climb the staircase.
//The order of the steps matters.

//For example, if N is 4, then there are 5 unique ways:
//1, 1, 1, 1
//2, 1, 1
//1, 2, 1
//1, 1, 2
//2, 2

//What if, instead of being able to climb 1 or 2 steps at a time, you could climb any number from a set of positive integers X?
//For example, if X = {1, 3, 5}, you could climb 1, 3, or 5 steps at a time. Generalize your function to take in X.