// Implement an autocomplete system.
// That is, given a query string s and a set of all possible query strings, return all strings in the set that have s as a prefix.
// For example, given the query string "de" and the set of strings ["dog", "deer", "deal"], return ["deer", "deal"].
// Hint: Try preprocessing the dictionary into a more efficient data structure to speed up queries.

using System.Linq;

namespace DailyCodingProblem.Problem011_Autocomplete
{
    public class Problem011
    {
        public string[] AutoComplete(string query, string[] words)
        {
            return words.Where(e => e.StartsWith(query)).ToArray();
        }

        public string[] AutoCompleteWithDictionary(string query, string[] words)
        {
            var dic = words
                .GroupBy(e => e.Substring(0, query.Length))
                .ToDictionary(e => e.Key, e => e.ToArray());

            return dic.TryGetValue(key: query, value: out var wordArray) 
                ? wordArray 
                : new string[0];
        }
    }
}