using System.Diagnostics;
using NUnit.Framework;

namespace DailyCodingProblem.Problem011_Autocomplete
{
    [TestFixture]
    public class Problem011UT
    {
        [Test]
        [TestCase("de", new[] {"dog", "deer", "deal"}, new []{"deer", "deal"})]
        [TestCase("de", new[] {"dog", "doge", "pineapple"}, new string[0])]
        public void test(string query, string[] words, string[] expected)
        {
            var sut = new Problem011();
            
            var result = sut.AutoComplete(query, words);
            
            Assert.AreEqual(expected, result);
            
            
            result = sut.AutoCompleteWithDictionary(query, words);
            
            Assert.AreEqual(expected, result);
        }
    }
}