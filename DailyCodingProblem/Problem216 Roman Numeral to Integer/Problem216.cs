﻿//Given a number in Roman numeral format, convert it to decimal.
//The values of Roman numerals are as follows:
//M: 1000
//D: 500
//C: 100
//L: 50
//X: 10
//V: 5
//I: 1

//IN addition, note that the Roman numeral system uses subtractive notation for numbers such as IV and XL.
//For the input XIV, for instance, you should return 14.
using System.Collections.Generic;
using System.Linq;

namespace DailyCodingProblem.Problem216
{
    public class Problem216
    {
        private readonly Dictionary<char, int> _romanNumeralToInteger = new Dictionary<char, int>
        {
            { 'M', 1000 },
            { 'D', 500 },
            { 'C', 100 },
            { 'L', 50 },
            { 'X', 10 },
            { 'V', 5 },
            { 'I', 1}
        };
        
        public int ToInteger(string romanNumeral)
        {
            var numbers = romanNumeral
                .Select(letter => _romanNumeralToInteger[letter])
                .ToArray();

            for (var i = 0; i < numbers.Length; i++)
            {
                if (i + 1 > numbers.Length - 1 || numbers[i + 1] <= numbers[i]) 
                    continue;
                
                numbers[i] = numbers[i + 1] - numbers[i];
                numbers[i + 1] = 0;
                i++;
            }

            return numbers.Sum();
        }
    }
}