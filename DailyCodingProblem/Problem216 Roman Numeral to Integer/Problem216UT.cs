﻿using NUnit.Framework;

namespace DailyCodingProblem.Problem216
{
    [TestFixture]
    public class Problem216UT
    {
        [Test]
        [TestCase("XIV", 14)]
        [TestCase("MMMMCDXCIV", 4494)]
        public void test(string input, int expected)
        {
            var sut = new Problem216();

            var result = sut.ToInteger(input);
            
            Assert.AreEqual(expected, result);
        }
    }
}