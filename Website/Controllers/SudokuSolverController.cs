using System.Linq;
using System.Web.Mvc;
using DailyCodingProblem.Problem054;
using Website.Extensions;
using Website.Models;

namespace Website.Controllers
{
    public class SudokuSolverController : Controller
    {
        public ActionResult Index(string errorMessage = null)
        {
            return View(new SudokuSolverModel()
            {
                Board = new char[81],
                ErrorMessage = errorMessage
            });
        }

        [HttpPost]
        public ActionResult SolveSudoku(char[] board)
        {
            var solver = new Problem054();
            var twoDimensionalBoard = board.To2DArray(9);

//            if (!BoardValidator.Validate(twoDimensionalBoard))
//            {
//                return RedirectToAction("Index", routeValues: "Not solveable.");
//            }
            
            var result = solver.SolveSudoku(twoDimensionalBoard);
            var model = new SudokuSolverModel()
            {
                IsSolved = result.IsSolved,
                Board = result.Board.Cast<char>().ToArray()
            };

            if (model.IsSolved)
            {
                return View("Index", model);
            }

            return RedirectToAction("Index", routeValues: "Not solveable.");
        }
    }
}