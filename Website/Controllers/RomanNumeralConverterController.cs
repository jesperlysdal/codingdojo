using System.Web.Mvc;
using CodingDojo.RomanNumbers;

namespace Website.Controllers
{
    public class RomanNumeralConverterController : Controller
    {
        private readonly RomanNumberConverter _converter;
        
        public RomanNumeralConverterController()
        {
            _converter = new RomanNumberConverter();
        }
        
        public ActionResult Index(string result)
        {
            return View(result);
        }

        [HttpPost]
        public ActionResult Convert(string romanNumeral)
        {
            var result = int.TryParse(romanNumeral, out var number) 
                ? _converter.ToRoman(number) 
                : _converter.ToInteger(romanNumeral).ToString();

            return View("Index", model: result);
        }
    }
}