using System.Linq;
using System.Web.Mvc;
using DailyCodingProblem;

namespace Website.Controllers
{
    public class DailyProblemController : Controller
    {
        public ActionResult Index(int? number)
        {
            if (number == null)
                return View();

            var problem = InterfaceImplementations
                .InstancesOf<IProblem>()
                .FirstOrDefault(e => e.Problem == number.Value);

            return problem == null 
                ? View() 
                : View(problem.Problem);
        }
    }
}