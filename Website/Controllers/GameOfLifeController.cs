using System.Threading;
using System.Web.Mvc;
using GameOfLife.Assets;
using Website.Models;

namespace Website.Controllers
{
    public class GameOfLifeController : Controller
    {
        private Game _game;
        public ActionResult Index()
        {
            _game = new Game(10,10);
            _game.SetCell(5,5, CellType.Live);
            _game.SetCell(5,6, CellType.Live);
            _game.SetCell(5,7, CellType.Live);
            _game.SetCell(6,6, CellType.Live);
            _game.SetCell(4,6, CellType.Live);
            
            var model = new GameOfLifeModel(_game.GetGrid(), _game);
        
            return View(model);
            
        }

        [HttpPost]
        public ActionResult NextGeneration(GameOfLifeModel model)
        {
            model.Game.ComputeNextGeneration();
            model.Grid = model.Game.GetGrid();

            return Json(model.Grid);
        }
    }
}