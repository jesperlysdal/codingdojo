namespace Website.Extensions
{
    public static class ArrayExtensions
    {
        public static char[,] To2DArray(this char[] array, int dimensions)
        {
            var count = 0;
            var result = new char[dimensions,dimensions];
            
            for (var i = 0; i < dimensions; i++)
            {
                for (var j = 0; j < dimensions; j++)
                {
                    if (count == array.Length) break;

                    result[i,j] = array[count];

                    count++;
                }
            }

            return result;
        }
    }
}