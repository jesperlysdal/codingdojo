using System;
using Website.UnoGame;

namespace Website.Extensions
{
    public static class EnumExtensions
    {
        public static PlayDirection GetNextEnum(this PlayDirection direction)
        {
            switch (direction)
            {
                case PlayDirection.Clockwise:
                    return PlayDirection.CounterClockwise;
                case PlayDirection.CounterClockwise:
                    return PlayDirection.Clockwise;
                default:
                    throw new IndexOutOfRangeException();
            }
        }
    }
}