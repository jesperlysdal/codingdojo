﻿using System;
using System.Collections.Generic;

namespace Website.Extensions
{
    public static class ListExtensions
    {
        private static readonly Random Random = new Random();  

        public static void Shuffle<T>(this IList<T> list)
        {
            var count = list.Count;
            while (count > 1) {
                count--;
                var randomNumber = Random.Next(count + 1);
                (list[randomNumber], list[count]) = (list[count], list[randomNumber]);
            }  
        }
    }
}