﻿using System.Collections.Generic;
using MongoDB.Driver;

namespace Website.Services
{
    public class MongoDb
    {
        private IMongoDatabase GetDatabase(string dbName = "local")
        {
            var dbClient = new MongoClient("mongodb://localhost:27017");

            return dbClient.GetDatabase(dbName);
        }

        public List<T> List<T>()
        {
            var database = GetDatabase();
            return database.GetCollection<T>(typeof(T).Name).AsQueryable().ToList();
        }
    }
}