﻿namespace Website.UnoGame
{
    public enum CardValue
    {
        Zero = 0,
        One = 1,
        Two = 2,
        Three = 3,
        Four = 4,
        Five = 5,
        Six = 6,
        Seven = 7,
        Eight = 8,
        Nine = 9,
        DrawTwo = 10,
        WildDrawFour = 11,
        Reverse = 12,
        Skip = 13,
        Wild = 14
    }
}