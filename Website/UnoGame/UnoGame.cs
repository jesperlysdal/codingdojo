﻿using System;
using System.Collections.Generic;
using System.Linq;
using Website.Extensions;

namespace Website.UnoGame
{
    public class UnoGame
    {
        private readonly Player[] _players;
        private int _playerIndexTurn = 0;
        private readonly Deck _deck;
        private PlayDirection _direction;
        private Card _lastCard;

        private readonly CardValue[] _specialCases = {CardValue.Wild, CardValue.WildDrawFour, CardValue.Reverse, CardValue.Skip};
        
        public UnoGame(Player[] players)
        {
            if (players.Length < 2 || players.Length > 10)
                throw new ArgumentException("Uno must be played with 2-10 players.");

            _direction = PlayDirection.Clockwise;
            _deck = new Deck();
            Console.WriteLine($"Deck consists of {_deck.Count()} cards. Official rules states it should be 108.");
            _players = players;
            _lastCard = _deck.Draw();

            foreach (var player in _players)
            {
                Deal(player);
            }
        }

        private void Deal(Player player)
        {
            for (var i = 0; i < 7; i++)
            {
                player.Hand.Add(_deck.Draw());
            }
        }

        public void Run()
        {
            while (_players.All(e => e.Hand.Any()))
            {
                var player = _players[_playerIndexTurn];

                if (_lastCard.Value == CardValue.DrawTwo)
                {
                    Console.WriteLine($"Drawing 2 extra cards for {player.Name}.");
                    for (var i = 0; i < 2; i++)
                    {
                        player.Hand.Add(_deck.Draw());
                    }
                }

                if (_lastCard.Value == CardValue.WildDrawFour)
                {
                    Console.WriteLine($"Drawing 4 extra cards for {player.Name} and switching to {_lastCard.Color}.");
                    for (var i = 0; i < 4; i++)
                    {
                        player.Hand.Add(_deck.Draw());
                    }
                }

                while (player.Options(_lastCard) == false)
                {
                    Console.WriteLine($"No options for player {player.Name}, drawing...");
                    player.Hand.Add(_deck.Draw());
                }
                Console.WriteLine($"{player.Name}s turn.");
                var currentHand = string.Join(", ", player.Hand.Select(e => $"{player.Hand.IndexOf(e)} {e}" ));
                Console.WriteLine($"Your hand: {currentHand}");
                Console.WriteLine($"Last card was {_lastCard}. Please select index of card you wish to play:");
                AwaitLegalPlay(player);

                _playerIndexTurn += (int) _direction;

                if (_playerIndexTurn > _players.Length - 1)
                    _playerIndexTurn -= _players.Length;

                if (_playerIndexTurn < 0)
                    _playerIndexTurn += _players.Length;
            }

            var winner = _players.First(e => e.Hand.Count == 0);
            Console.WriteLine($"{winner.Name} wins. Congratulations! You forgot to say UNO though ;)");
        }

        private void AwaitLegalPlay(Player player)
        {
            var success = false;
            var input = -1;

            while (success == false)
            {
                Console.WriteLine($"Please pick a valid card to play...");
                var read = Console.ReadLine();
                if (int.TryParse(read, out input))
                {
                    success = LegalPlay(player.Play(input));
                }
            }

            _lastCard = player.Hand[input];
            
            if (_specialCases.Contains(_lastCard.Value))
            {
                if (_lastCard.Value == CardValue.Wild || _lastCard.Value == CardValue.WildDrawFour)
                {
                    success = false;
                    while (success == false)
                    {
                        Console.WriteLine($"Which color would you like to switch to? Red = 0, Green = 1, Blue = 2, Yellow = 3.");
                        var read = Console.ReadLine();
                        if (int.TryParse(read, out input) && 
                            input <= 3 && 
                            input >= 0)
                        {
                            _lastCard.Color = (CardColor) input;
                            success = true;
                        }
                    }
                }
                HandleSpecialCase(_lastCard);
            }
                
            player.Hand.RemoveAt(input);
        }

        private void HandleSpecialCase(Card lastCard, CardColor? colorChoice = null)
        {
            switch (lastCard.Value)
            {
                case CardValue.Reverse:
                    _direction = _direction.GetNextEnum();
                    break;
                case CardValue.Skip:
                    var value = (int) _direction * _playerIndexTurn + (int)_direction;
                    _playerIndexTurn = value < 0
                        ? value + _playerIndexTurn
                        : value;
                    break;
                case CardValue.Wild:
                    if (colorChoice == null) throw new ArgumentException();
                    _lastCard.Color = colorChoice.Value;
                    break;
                case CardValue.WildDrawFour:
                    break;
                
            }
        }

        private bool LegalPlay(Card playedCard)
        {
            if (playedCard.Color == _lastCard.Color ||
                playedCard.Value == _lastCard.Value ||
                playedCard.Value == CardValue.Wild || 
                playedCard.Value == CardValue.WildDrawFour)
                return true;
            
            return false;
        }
    }
}