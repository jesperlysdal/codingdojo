﻿using System.Collections.Generic;
using System.Linq;

namespace Website.UnoGame
{
    public class Player
    {
        public Player(string name)
        {
            Name = name;
            Hand = new List<Card>();
        }
        
        public string Name { get; }
        public List<Card> Hand { get; private set; }

        public Card Play(int index)
        {
            return Hand[index];
        }

        public bool Options(Card lastCard)
        {
            var handValues = Hand.Select(e => e.Value).ToArray();
            if (Hand.All(e => e.Color != lastCard.Color) && 
                (!handValues.Contains(CardValue.Wild) || !handValues.Contains(CardValue.WildDrawFour)))
                return false;

            return true;
        }
    }
}