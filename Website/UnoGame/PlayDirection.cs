﻿namespace Website.UnoGame
{
    public enum PlayDirection
    {
        CounterClockwise = -1,
        Clockwise = 1
    }
}