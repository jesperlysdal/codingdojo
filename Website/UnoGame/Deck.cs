﻿using System;
using System.Collections.Generic;
using System.Linq;
using Website.Extensions;

namespace Website.UnoGame
{
    public class Deck
    {
        private List<Card> Cards { get; set; }
        public Deck()
        {
            Build();
            Cards.Shuffle();
        }

        public int Count()
        {
            return Cards.Count;
        }

        private void Build()
        {
            Cards = new List<Card>();

            AddCards(new[] {CardValue.Wild, CardValue.WildDrawFour});
            AddCards(new[] {CardValue.Wild, CardValue.WildDrawFour, CardValue.Zero});

            for (var i = 0; i < 4; i++)
            {
                Cards.Add(new Card(CardColor.None, CardValue.Wild));
                Cards.Add(new Card(CardColor.None, CardValue.WildDrawFour));
            }
        }

        private void AddCards(CardValue[] except)
        {
            var cardColors = Enum.GetValues(typeof(CardColor)).Cast<CardColor>().ToArray();
            var cardValues = Enum.GetValues(typeof(CardValue)).Cast<CardValue>().ToArray();
            foreach (var cardColor in cardColors.Where(e => e != CardColor.None))
            {
                foreach (var cardValue in cardValues.Where(e => !except.Contains(e)))
                {
                    var cardToAdd = new Card(cardColor, cardValue);
                    Cards.Add(cardToAdd);
                }
            }
        }

        public Card Draw()
        {
            var idx = Cards.Count - 1;
            var toReturn = Cards[idx];
            Cards.RemoveAt(idx);

            return toReturn;
        }
    }
}