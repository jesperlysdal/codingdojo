﻿namespace Website.UnoGame
{
    public enum CardColor
    {
        Red = 0,
        Green = 1,
        Blue = 2,
        Yellow = 3,
        None = 4
    }
}