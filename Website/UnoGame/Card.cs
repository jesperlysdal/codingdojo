﻿namespace Website.UnoGame
{
    public class Card
    {
        public Card(CardColor color, CardValue value)
        {
            Color = color;
            Value = value;
        }
        
        public CardColor Color { get; set; }
        public CardValue Value { get; }

        public override string ToString()
        {
            var color = Color == CardColor.None ? "" : Color.ToString();
            return $"{color} {Value}";
        }
    }
}