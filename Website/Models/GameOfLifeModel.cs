using GameOfLife.Assets;

namespace Website.Models
{
    public class GameOfLifeModel
    {
        public GameOfLifeModel(CellType[][] grid, Game game)
        {
            Grid = grid;
            Game = game;
        }

        public GameOfLifeModel()
        {
            
        }
        
        public CellType[][] Grid { get; set; }
        public Game Game { get; set; }
    }
}