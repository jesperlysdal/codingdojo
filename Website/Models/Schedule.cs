﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Website.Models
{
    public class Schedule
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public DayOfWeek DayOfWeek { get; set; }
        public bool EvenWeeks { get; set; }
        public string Task { get; set; }
    }
}