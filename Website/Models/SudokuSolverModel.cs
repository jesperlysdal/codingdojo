namespace Website.Models
{
    public class SudokuSolverModel
    {
        public bool IsSolved { get; set; }
        public char[] Board { get; set; }
        public string ErrorMessage { get; set; }
    }
}