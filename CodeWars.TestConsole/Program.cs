﻿using System;
using System.Diagnostics;

namespace CodeWars.TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var test = NumberShorteningFilter.ShortenNumberCreator(new [] { "", "k", "m" }, 1000);
            var result = test("32424234223");
        }

        private static void BattleShips()
        {
            var field = new int[10, 10]
            {
                { 1, 0, 0, 0, 0, 1, 1, 0, 0, 0 },
                { 1, 0, 1, 0, 0, 0, 0, 0, 1, 0 },
                { 1, 0, 1, 0, 1, 1, 1, 0, 1, 0 },
                { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                { 0, 0, 0, 0, 1, 1, 1, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                { 0, 0, 0, 1, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
            };

            var test = BattleshipField.ValidateBattlefield(field);
        }
    }
}