using NUnit.Framework;

namespace AdventOfCode2019.Day1
{
    [TestFixture]
    public class Day1UT
    {
        [Test]
        public void test()
        {
            var result = Day1.FuelRequirements();
            
            Assert.AreEqual(3249140, result);
        }
        
        [Test]
        public void test_part_two()
        {
            var result = Day1.FuelRequirementsPart2();
            
            Assert.AreEqual(4870838, result);
        }
    }
}