using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode2019.Day1
{
    public class Day1
    {
        public static int FuelRequirements()
        {
            return File.ReadAllLines("C:\\Users\\Jesper\\source\\repos\\codingdojo\\AdventOfCode2019\\Day1\\input.txt")
                .Select(int.Parse)
                .Select(e => e / 3 - 2)
                .Sum();
        }
        
        public static int FuelRequirementsPart2()
        {
            var moduleMasses = File.ReadAllLines("C:\\Users\\Jesper\\source\\repos\\codingdojo\\AdventOfCode2019\\Day1\\input.txt")
                .Select(int.Parse);
                
            var fuelRequirements = new List<int>();

            foreach (var moduleMass in moduleMasses)
            {
                var temp = moduleMass;
                while (temp > 0)
                {
                    temp = temp / 3 - 2;
                    
                    if (temp > 0)
                        fuelRequirements.Add(temp);
                }
            }
                
            return fuelRequirements.Sum();
        }
    }
}