namespace AdventOfCode2019.Day2
{
    public class Day2
    {
        public static int[] Intcode(int[] input)
        {
            for (var i = 0; i < input.Length; i += 4)
            {
                switch (input[i])
                {
                    case 99:
                        return input;
                    case 1:
                        Add(input, i);
                        break;
                    case 2:
                        Multiply(input, i);
                        break;
                    default:
                        return input;
                }
            }

            return input;
        }

        private static void Multiply(int[] input, int i)
        {
            input[input[i+3]] = input[input[i+1]] * input[input[i+2]];
        }

        private static void Add(int[] input, int i)
        {
            input[input[i+3]] = input[input[i+1]] + input[input[i+2]];
        }
    }
}