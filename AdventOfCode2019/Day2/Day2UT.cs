using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace AdventOfCode2019.Day2
{
    [TestFixture]
    public class Day2UT
    {
        [Test]
        [TestCase(new[]{1,0,0,0,99}, new[]{2,0,0,0,99})]
        [TestCase(new[]{2,3,0,3,99}, new[]{2,3,0,6,99})]
        [TestCase(new[]{2,4,4,5,99,0}, new[]{2,4,4,5,99,9801})]
        [TestCase(new[]{1,1,1,4,99,5,6,0,99}, new[]{30,1,1,4,2,5,6,0,99})]
        public void test(int[] input, int[] expected)
        {
            var result = Day2.Intcode(input);
            
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void test_solution()
        {
            var input = File.ReadAllText("C:\\Users\\Jesper\\source\\repos\\codingdojo\\AdventOfCode2019\\Day2\\input.txt")
                .Split(',')
                .Select(int.Parse)
                .ToArray();

            var result = Day2.Intcode(input);
            
            Assert.AreEqual(new[]{250702, 0, 0, 2, 1, 1, 2, 3, 1, 3, 4, 3, 1, 5, 0, 3, 2, 1, 9, 0, 1, 19, 5, 1, 2, 6, 23, 2, 1, 6, 27, 4, 2, 31, 9, 12, 1, 35, 6, 14, 1, 10, 39, 18, 2, 9, 43, 54, 1, 5, 47, 55, 2, 51, 6, 110, 1, 5, 55, 111, 2, 13, 59, 555, 1, 63, 5, 556, 2, 67, 13, 2780, 1, 71, 9, 2783, 1, 75, 6, 2785, 2, 79, 6, 5570, 1, 83, 5, 5571, 2, 87, 9, 16713, 2, 9, 91, 50139, 1, 5, 95, 50140, 2, 99, 13, 250700, 1, 103, 5, 250701, 1, 2, 107, 250701, 1, 111, 5, 0, 99, 2, 14, 0, 0}, result);
        }
        
        [Test]
        public void actual_solution()
        {
            var input = File.ReadAllText("C:\\Users\\Jesper\\source\\repos\\codingdojo\\AdventOfCode2019\\Day2\\input.txt")
                .Split(',')
                .Select(int.Parse)
                .ToArray();

            input[1] = 12;
            input[2] = 2;

            var result = Day2.Intcode(input);
            
            Assert.AreEqual(new[]{3166704,12,2,2,1,1,2,3,1,3,4,3,1,5,0,3,2,1,9,36,1,19,5,37,2,6,23,74,1,6,27,76,2,31,9,228,1,35,6,230,1,10,39,234,2,9,43,702,1,5,47,703,2,51,6,1406,1,5,55,1407,2,13,59,7035,1,63,5,7036,2,67,13,35180,1,71,9,35183,1,75,6,35185,2,79,6,70370,1,83,5,70371,2,87,9,211113,2,9,91,633339,1,5,95,633340,2,99,13,3166700,1,103,5,3166701,1,2,107,3166703,1,111,5,0,99,2,14,0,0}, result);
        }

        [Test]
        public void part_two()
        {

            var noun = 0;
            var verb = 0;

            var results = new List<int>();

            var result = 0;
            
            for (var i = 0; i < 100; i++)
            {
                for (var j = 0; j < 100; j++)
                {
                    var input = File.ReadAllText("C:\\Users\\Jesper\\source\\repos\\codingdojo\\AdventOfCode2019\\Day2\\input.txt")
                        .Split(',')
                        .Select(int.Parse)
                        .ToArray();
                    
                    input[1] = i;
                    input[2] = j;
                    result = Day2.Intcode(input)[0];
                    results.Add(result);
                    if (result == 19690720)
                    {
                        noun = i;
                        verb = j;
                        break;
                    }
                }
            }
            
            
            Assert.AreEqual(19690720, result);
        }
    }
}