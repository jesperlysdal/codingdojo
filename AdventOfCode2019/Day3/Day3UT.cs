using NUnit.Framework;

namespace AdventOfCode2019.Day3
{
    [TestFixture]
    public class Day3UT
    {
        [Test]
        public void test()
        {
            var sut = new Day3();

            var result = sut.ManhattanDistance();
            
            Assert.AreEqual(3, result);
        }
    }
}