﻿namespace CodeWars
{
    public class HelloJohnny
    {
        // https://www.codewars.com/kata/55225023e1be1ec8bc000390/train/csharp
        
        public static string greet(string name)
        {
            if (name == "Johnny")
                return "Hello, my love!";
            
            return "Hello, " + name + "!";
        }
    }
}