﻿using System;
using System.Collections.Generic;

namespace CodeWars
{
    //https://www.codewars.com/kata/563fbac924106b8bf7000046/train/csharp
    public class BreadCrumbGenerator
    {
        // Kata.GenerateBC("www.codewars.com/users/GiacomoSorbi", " / ") == '<a href="/">HOME</a> / <a href="/users/">USERS</a> / <span class="active">GIACOMOSORBI</span>';
        public static string GenerateBreadcrumb(string url, string separator)
        {
            if (string.IsNullOrEmpty(url))
                return "";
            
            url = url
                .Replace("http://", "")
                .Replace("https://", "");

            var startIndex = url.IndexOf("/", StringComparison.Ordinal); // 16

            if (startIndex == -1)
                return url;

            var breadcrumbs = new List<string>();
            
            var root = url.Substring(0, startIndex); // www.codewars.com
            breadcrumbs.Add($"<a href=\"/\">HOME</a>");
            var relativeUrl = url.Substring(startIndex); // /users/GiacomoSorbi

            var relatives = relativeUrl.Split("/");
            
            
            
            
            
            
            // Do the things
            return string.Join($" {separator.Trim()} ", breadcrumbs);
        }
    }
}