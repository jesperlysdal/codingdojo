﻿using System;
using System.Linq;

namespace CodeWars
{
    // https://www.codewars.com/kata/56b4af8ac6167012ec00006f/train/csharp
    
    // Examples:
    // var filter1 = shortenMethods.ShortenNumberCreator(new string[] { "", "k", "m" }, 1000);
    // filter1("234324") == "234k";
    // filter1("98234324") == "98m";
    // filter1(new int[] { 1,2,3 }) == "1,2,3";
    // var filter2 = shortenMethods.ShortenNumberCreator(new string[] { "B", "KB", "MB", "GB" }, 1024);
    // filter2("32") == "32B";
    // filter2("2100") == "2KB";
    // filter2("pippi") == "pippi";
    
    public class NumberShorteningFilter
    {
        public static Func<object, string> ShortenNumberCreator(string[] suffixes, int baseValue)
        {
            return e =>
            {
                if (e.GetType() == typeof(int[]))
                    return string.Join(",", (int[])e);
                
                var str = e.ToString();
                if (!int.TryParse(str, out var result))
                    return $"{str}";

                if (result / baseValue <= 0)
                    return $"{result.ToString()}{suffixes.FirstOrDefault()}";
                
                foreach (var suffix in suffixes.Skip(1))
                {
                    if (result / baseValue <= 0)
                        break;
                    
                    result /= baseValue;
                    str = $"{result}{suffix}";
                }
                
                return str;
            };
        }
    }
}