﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeWars
{
    // https://www.codewars.com/kata/5739174624fc28e188000465/train/csharp
    public enum Result 
    { 
        Win, 
        Loss, 
        Tie 
    }

    public enum Hand
    {
        HighCard,
        Pair,
        TwoPairs,
        ThreeOfAKind,
        Straight,
        Flush,
        FullHouse,
        FourOfAKind,
        StraightFlush,
        RoyalFlush
    }

    
    // The characteristics of the string of cards are:
    //
    // Each card consists of two characters, where
    // The first character is the value of the card: 2, 3, 4, 5, 6, 7, 8, 9, T(en), J(ack), Q(ueen), K(ing), A(ce)
    // The second character represents the suit: S(pades), H(earts), D(iamonds), C(lubs)
    // A space is used as card separator between cards
    // ex. "KS 2H 5C JD TD"
    public class PokerHand
    {
        public readonly (Hand Hand, int Score) Score;

        public PokerHand(string hand)
        {
            var cards = hand.Split(' ');
            var values = cards.Select(e => e.First()).ToArray();
            var suits = cards.Select(e => e.Last()).ToArray();
            
            if (CheckFlush(suits))
            {
                // flush - figure if straight flush, else give high card as value

                Score = CheckStraight(values) 
                    ? (Hand.StraightFlush, values.Max()) 
                    : (Hand.Flush, values.Max());
            }
        }

        private static bool CheckFlush(char[] suits)
        {
            return suits.Distinct().Count() == 1;
        }

        private static bool CheckStraight(char[] values)
        {
            return values
                .Zip(values.Skip(1), (a, b) => Math.Abs(a - b) == 1)
                .All(b => b);
        }

        public Result CompareWith(PokerHand hand)
        {
            if (hand.Score.Hand > Score.Hand)
                return Result.Loss;
            
            if (hand.Score.Hand == Score.Hand && hand.Score.Score == Score.Score)
                return Result.Tie;

            return Result.Win;
        }
    }
}