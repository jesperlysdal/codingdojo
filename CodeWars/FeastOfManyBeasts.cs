﻿using System.Linq;

namespace CodeWars
{
    public class FeastOfManyBeasts
    {
        // https://www.codewars.com/kata/5aa736a455f906981800360d/train/csharp
        
        public static bool Feast(string beast, string dish)
        {
            return beast.First() == dish.First() && beast.Last() == dish.Last();
        }
    }
}