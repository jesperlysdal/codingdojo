﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeWars
{
    public class Challenges
    {
        public static int[] ArrayDiff(int[] a, int[] b)
        {
            var bHashSet = b.ToHashSet();

            return a.Where(current => !bHashSet.Contains(current)).ToArray();
        }
        
        public static int SquareDigits(int n)
        {
            var sb = new StringBuilder();
            foreach (var c in n.ToString())
            {
                var no = int.Parse(c.ToString());

                var temp = Math.Pow(no, 2);
                sb.Append(temp);
            }

            return int.Parse(sb.ToString());
        }
        
        public static string HighAndLow(string numbers)
        {
            var split = numbers.Split(' ').Select(int.Parse).ToArray();
                
            return $"{split.Max()} {split.Min()}";
        }
        
        public static bool XO(string input)
        {
            return input.Count(e => e is 'o' or 'O') == input.Count(e => e is 'x' or 'X');
        }
        
        public static string PrinterError(String s)
        {
            var errors = s.Count(e => e > 'm');

            return $"{errors}/{s.Length}";
        }
        
        public static int CountBits(int n)
        {
            return Convert.ToString(n, 2).Count(e => e == '1');
        }
        
        public static string Maskify(string cc)
        {
            var len = cc.Length;
            return len <= 4 
                ? cc 
                : cc.Substring(len - 4).PadLeft(len, '#');
        }
        
        public static string SongDecoder(string input)
        {
            return string.Join(" ", input.Split("WUB", StringSplitOptions.RemoveEmptyEntries));
        }
        
        public static int sumTwoSmallestNumbers(int[] numbers)
        {
            if (numbers.Length < 2)
                throw new ArgumentException();

            return numbers.OrderBy(e => e).Take(2).Sum();
        }

        public static bool IsValidTriangle(int a, int b, int c)
        {
            return a + b > c &&
                   a + c > b &&
                   b + c > a;
        }
        
        public static string CreatePhoneNumber(int[] numbers)
        {
            var s = string.Join("", numbers.Select(e => e.ToString()));
            return $"({s.Substring(0, 3)}) {s.Substring(3, 3)}-{s.Substring(6, 4)}";
        }
        
        public static bool ValidParentheses(string input)
        {
            var start = 0;
            foreach (var c in input)
            {
                switch (c)
                {
                    case '(':
                        start++;
                        break;
                    case ')' when start == 0:
                        return false;
                    case ')':
                        start--;
                        break;
                }
            }

            return start == 0;
        }
        
        public static bool IsPangram(string str)
        {
            return str
                .ToLower()
                .Where(Char.IsLetter)
                .Distinct()
                .Count() == 26;
        }
        
        public static string AlphabetPosition(string text)
        {
            return string.Join(" ", text.Where(char.IsLetter).Select(e => char.ToUpper(e) - 64));
        }
        
        public static IEnumerable<T> UniqueInOrder<T>(IEnumerable<T> iterable)
        {
            var result = new List<T>();
            T current = default;
            foreach (var e in iterable)
            {
                if (!e.Equals(current))
                    result.Add(e);
                
                current = e;
            }

            return result;
        }
        
        public static string[] TowerBuilder(int nFloors)
        {
            var tower = new string[nFloors];
            var longestRow = nFloors + nFloors - 1;
            
            for (var i = 1; i <= nFloors; i++)
            {
                tower[i-1] = new string('*', i + i - 1)
                    .PadLeft((longestRow-1) / 2, ' ')
                    .PadRight((longestRow-1) / 2, ' ');
            }
            
            return tower;
        }
    }
}