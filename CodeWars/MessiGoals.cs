﻿namespace CodeWars
{
    public class MessiGoals
    {
        // https://www.codewars.com/kata/55ca77fa094a2af31f00002a
        
        public static int la_liga_goals = 43;
        public static int champions_league_goals = 10;
        public static int copa_del_rey_goals = 5;
        public static int total_goals => la_liga_goals + champions_league_goals + copa_del_rey_goals;


    }
}