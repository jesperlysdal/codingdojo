﻿using System.Collections.Generic;
using System.Linq;

namespace CodeWars
{
    public class BattleshipField
    {
        public static bool ValidateBattlefield(int[,] field)
        {
            var shipsNeeded = new List<int>() { 4, 3, 3, 2, 2, 2, 1, 1, 1, 1 };
            var fieldLength = field.GetLength(0);
            for (int i = 0; i < fieldLength; i++)
            {
                for (int j = 0; j < fieldLength; j++)
                {
                    if (field[i, j] == 1)
                    {
                        var shipLength = 1;
                        for (int k = 1; k < 4; k++)
                        {
                            if (i + k < fieldLength && field[i + k, j] == 1 || 
                                j + k < fieldLength && field[i, j + k] == 1)
                                shipLength += 1;
                        }

                        var toRemove = shipsNeeded.FirstOrDefault(e => e == shipLength);

                        if (toRemove == 0)
                            return false;

                        shipsNeeded.Remove(toRemove);
                    }
                }
            }

            return !shipsNeeded.Any();
        }
    }
}