﻿namespace CodeWars
{
    public class BecomeImmortal
    {
        public static long ElderAge(long n, long m, long k, long newp)
        {
            long seconds = 0;
            for (var i = 0; i < n; i++)
            {
                for (var j = 0; j < m; j++)
                {
                    var result = i ^ j;
                    if (result > k)
                        seconds += (result - k);
                }
            }
            
            return seconds % newp;
        }
    }
}