﻿using System;

namespace CodeWars
{
    // https://www.codewars.com/kata/51fda2d95d6efda45e00004e/train/csharp
    public class User
    {
        // Scores
        // Completing activity rank = user rank awards 3 points
        // Completing activity rank = user rank - 1 awards 1 point. Any activity rank below user rank - 1 will be ignored.
        // Completing activity rank higher than user rank will accelerate progression by 10 * d * d, where d is the difference in ranks.
        
        // If a user ranked -8 completes an activity ranked -7 they will receive 10 progress
        // If a user ranked -8 completes an activity ranked -6 they will receive 40 progress
        // If a user ranked -8 completes an activity ranked -5 they will receive 90 progress
        // If a user ranked -8 completes an activity ranked -4 they will receive 160 progress, resulting in the user being upgraded to rank -7 and having earned 60 progress towards their next rank
        // If a user ranked -1 completes an activity ranked 1 they will receive 10 progress (remember, zero rank is ignored)
        
        public int Rank { get; set; } // goes from -8 to 8, 0 exclusive
        public int Progress { get; set; } // 0-100, when 100 -> level up rank and start from 0. Any excess rank earned > 100 to be transferred over to next rank.

        public User()
        {
            Rank = -8;
            Progress = 0;
        }

        public void IncProgress(int activityRank)
        {
            if (Rank == activityRank)
            {
                Progress += 10;
            }
            
            if (Rank - 1 == activityRank)
            {
                Progress += 1;
            }

            if (Rank < activityRank)
            {
                var d = Math.Abs(activityRank - Rank);
                Progress += 10 * d * d;
            }

            while (Progress >= 100)
            {
                Rank += 1;
                if (Rank == 0)
                {
                    Rank += 1;
                }
                Progress -= 100;
            }
        }
    }
}