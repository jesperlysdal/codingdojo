﻿using System.Collections.Generic;
using System.Linq;

namespace CodeWars
{
    public class DblLinearKata
    {
        // https://www.codewars.com/kata/5672682212c8ecf83e000050/train/csharp
        public static int DblLinear (int n)
        {
            var u = new HashSet<int>(){ 1 };

            var idx = 0;

            while (u.Count <= n + n)
            {
                var value = u.ToList()[idx];
                var y = 2 * value + 1;
                var z = 3 * value + 1;
                u.Add(y);
                u.Add(z);
                
                u = u.OrderBy(e => e)
                    .ToHashSet();
                
                idx++;
            }

            
            return u.ToList()[n];
        }
    }
}