﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeWars
{
    public class MorseDecoder
    {
        public static string Decode(string morseCode)
        {
            const string charSeparator = " ";
            const string wordSeparator = "   ";
            return string.Join(" ", morseCode.Trim().Split(wordSeparator)
                .Select(e => string.Join("", e.Split(charSeparator).Select(x => x))));
        }
        
        public static string DecodeBits(string bits)
        {
            var letters = new Dictionary<int, string>() { { 1, "." }, { 3, "-" } };
            var separators = new Dictionary<int, string>() { { 1, "" }, { 3, " " }, { 7, "   " } };
            var rate = TransmissionRate(bits);
            var sb = new StringBuilder();

            var zeros = bits.Split("1", StringSplitOptions.RemoveEmptyEntries);
            var ones = bits.Split("0", StringSplitOptions.RemoveEmptyEntries);

            for (var i = 0; i < ones.Length; i++)
            {
                var noOfOnes = ones[i].Length / rate;

                sb.Append(letters[noOfOnes]);

                if (i < zeros.Length)
                {
                    var noOfZeros = zeros[i].Length / rate;
                    sb.Append(separators[noOfZeros]);
                }
            }

            return sb.ToString();
        }

        private static int TransmissionRate(string code)
        {
            var rate = code.Split("0", StringSplitOptions.RemoveEmptyEntries);

            return rate.Min(e => e.Length);
        }
    }
}