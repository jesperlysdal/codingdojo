using System.Diagnostics;
using System.IO;
using NUnit.Framework;

namespace CodingDojo.LargestTriangle
{
    [TestFixture]
    public class LargestTriangleFinderUT
    {
        [Test]
        [TestCase(12.5d, new []{"1,1", "1,5", "6,1", "6,6"})]
        [TestCase(1, new []{"1,1", "2,2", "3,3", "3,4"})]
        [TestCase(13.5d, new []{"1,1", "1,1", "1,10", "4,5"})]
        // [TestCase(216623130.24789476d)]
        public void test(double expected, string[] input = null)
        {
            if (input == null)
            {
                input = File.ReadAllLines("D:\\projects\\codingdojo\\CodingDojo\\LargestTriangle\\input.txt");;
            }
            
            var sut = new LargestTriangleFinder();
            
            var sw = new Stopwatch();
            sw.Start();
            var result = sut.Find(input);
            sw.Stop();
            
            Assert.AreEqual(expected, result);
            Assert.Less(sw.Elapsed.TotalMilliseconds, 1000);
        }
    }
}