// Givet n punkter skal du beregne arealet af den største trekant udspændt af tre af punkterne.

using System;
using System.Collections.Generic;
using System.Linq;

namespace CodingDojo.LargestTriangle
{
    public class LargestTriangleFinder
    {
        public double Find(string[] input)
        {
            var coordinates = new List<(int, int)>();
            var areas = new List<double>();

            foreach (var line in input)
            {
                var split = line.Split(',');;
                coordinates.Add((int.Parse(split[0]), int.Parse(split[1])));
            }

            while (coordinates.Count > 2)
            {
                for (var i = 1; i < coordinates.Count - 2; i++)
                {
                    for (var j = 2; j < coordinates.Count - 2; j++)
                    {
                        if (coordinates[0] == coordinates[i] || 
                            coordinates[0] == coordinates[j] || 
                            coordinates[i] == coordinates[j])
                            continue;
                    
                        var a = Length(coordinates[0], coordinates[i + 1]);
                        var b = Length(coordinates[0], coordinates[i + 2]);
                        var c = Length(coordinates[i + 1], coordinates[i + 2]);

                        var area = Area(a, b, c);
                        areas.Add(area);
                    }
                }
                coordinates.RemoveAt(0);
            }

            return areas.Max();
        }

        private double Area(double sideA, double sideB, double sideC)
        {
            //Herons formula
            var s = (sideA + sideB + sideC) / 2.0;
            return Math.Sqrt(s * (s - sideA) * (s - sideB) * (s - sideC));
        }

        private double Length((int,int) xCoordinate, (int,int) yCoordinate)
        {
            var a = Math.Pow(xCoordinate.Item2 - xCoordinate.Item1, 2);
            var b = Math.Pow(yCoordinate.Item2 - yCoordinate.Item1, 2);
            return Math.Sqrt(a + b);
        }
    }
}