﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodingDojo.Palindrome.Program;

namespace CodingDojo
{
    public class PalindromeCalculator
    {
        public bool ValidatePalindrome(string input)
        {
            var reverse = new string(input.Reverse().ToArray());
            return reverse == input;
        }

        public bool ValidatePalindrome(int input)
        {
            return ValidatePalindrome(input.ToString());
        }

        public PalindromeProduct GetLargestPalindrome(int digits)
        {
            var upperLimit = GetUpperLimit(digits);
            var lowerLimit = GetLowerLimit(digits);
            var tempLowerLimit = upperLimit - lowerLimit;

            //split this bit up in bits i.e. iterate upperlimit - 1000, if maxproduct then
            //has value, no need to go on.

            return FindMaxPalindromeProduct(upperLimit, tempLowerLimit);
        }

        private PalindromeProduct FindMaxPalindromeProduct(int upperLimit, int lowerLimit)
        {
            var maxProduct = new PalindromeProduct();

            for (int firstFactor = upperLimit; firstFactor >= lowerLimit; firstFactor--)
            {
                for (int secondFactor = firstFactor; secondFactor >= lowerLimit; secondFactor--)
                {
                    var product = firstFactor * secondFactor;
                    if (product < maxProduct.Value)
                        break;
                    if (ValidatePalindrome(product))
                    {
                        maxProduct.Value = product;
                        maxProduct.OperatorA = firstFactor;
                        maxProduct.OperatorB = secondFactor;
                    }
                }
                if (maxProduct.Value > 0)
                    break;
            }
            return maxProduct;
        }

        public int GetUpperLimit(int digits)
        {
            return (int)Math.Pow(10, digits) - 1;
        }

        public int GetLowerLimit(int digits)
        {
            return (int)Math.Pow(10, digits - 1);
        }
    }
}
