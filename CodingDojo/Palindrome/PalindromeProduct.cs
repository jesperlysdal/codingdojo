﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingDojo.Palindrome.Program
{
    public class PalindromeProduct
    {
        public PalindromeProduct()
        {

        }

        public int Value { get; set; }
        public int OperatorA { get; set; }
        public int OperatorB { get; set; }
    }
}
