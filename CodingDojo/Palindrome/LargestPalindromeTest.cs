﻿using System;
using NUnit.Framework;

namespace CodingDojo.Palindrome
{
    public class LargestPalindromeTest
    {
        private PalindromeCalculator _sut;

        [Test]
        public void IsPalindrome()
        {
            _sut = new PalindromeCalculator();
            var result = _sut.ValidatePalindrome("aba");

            Assert.IsTrue(result);
        }

        [Test]
        public void IsNotPalindrome()
        {
            _sut = new PalindromeCalculator();
            var result = _sut.ValidatePalindrome("abb");

            Assert.IsFalse(result);
        }

        [Test]
        public void CanValidateInteger()
        {
            _sut = new PalindromeCalculator();
            var result = _sut.ValidatePalindrome(121);

            Assert.IsTrue(result);
        }

        [Test]
        public void CanDetermineUpperLimit()
        {
            _sut = new PalindromeCalculator();
            var result = _sut.GetUpperLimit(digits: 2);

            Assert.AreEqual(99, result);
        }

        [Test]
        public void CanDetermineLowerLimit()
        {
            _sut = new PalindromeCalculator();
            var result = _sut.GetLowerLimit(digits: 3);

            Assert.AreEqual(100, result);
        }

        [Test]
        public void ReturnsLargestPalindrome()
        {
            _sut = new PalindromeCalculator();
            var result = _sut.GetLargestPalindrome(digits: 3);

            Assert.AreEqual(expected: 906609, actual: result.Value);
        }
    }
}
