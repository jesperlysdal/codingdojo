﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingDojo.LargestProductInASeries
{
    public class SeriesProductCalculator
    {
        public Result LargestProduct(int digits, string input)
        {
            var charArray = input.ToCharArray();
            var result = new List<Result>();

            for (var i = 0; i < input.Length - digits; i++)
            {
                var tempResult = 1L;
                var ints = charArray
                    .Skip(i)
                    .Take(digits)
                    .Select(e => long.Parse(e.ToString()))
                    .ToArray();

                foreach (var number in ints)
                {
                    tempResult *= number;
                }
                result.Add(new Result(ints, tempResult));
            }
            var maxProduct = result.Select(e => e.Product).Max();
            return result.First(e => e.Product == maxProduct);
        }
    }

    public class Result
    {
        public long[] Numbers { get; set; }
        public long Product { get; set; }

        public Result(long[] numbers, long product)
        {
            Numbers = numbers;
            Product = product;
        }
    }
}
