﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace CodingDojo.LargeSum
{
    public class LargeSumCalculator
    {
        public BigInteger Sum(BigInteger[] numbers)
        {
            var result = new BigInteger(0);
            foreach (var number in numbers)
            {
                result += number;
            }
            return result;
        }
    }
}
