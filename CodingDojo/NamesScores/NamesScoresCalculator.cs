﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace CodingDojo.NamesScores
{
    public class NamesScoresCalculator
    {
        private readonly List<string> _alphabet = new List<string>(){"dummy", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
        private const string NamesFilePath = "D:/p022_names.txt";

        public List<string> GetSortedListOfNames()
        {
            var sr = new StreamReader(NamesFilePath);
            var namesString = sr.ReadToEnd();
            namesString = namesString.Replace('\"', ' ');

            var names = namesString
                .Split(',')
                .Select(e => e.Trim())
                .OrderBy(e => e)
                .ToList();

            return names;
        }

        public int GetAlphabeticalValue(string input)
        {
            var chars = input
                .ToLower()
                .ToCharArray()
                .Select(e => e.ToString())
                .ToArray();

            return chars.Select(e => _alphabet.IndexOf(e)).Sum();
        }

        public BigInteger SumNamesScores()
        {
            var names = GetSortedListOfNames();
            BigInteger result = 0;

            foreach (var name in names)
            {
                var alphabeticalValue = GetAlphabeticalValue(name);
                var numberInList = (names.IndexOf(name) + 1);
                var nameScore = alphabeticalValue * numberInList;
                result += nameScore;
            }

            return result;
        }

        public int Solution(int A)
        {
            if (A < 10)
                return A;

            var intArray = A
                .ToString()
                .ToCharArray()
                .Select(e => int.Parse(e.ToString()))
                .ToArray();

            var result = intArray.Select(e => e.ToString()).ToList();

            var startIndex = 0;
            var endIndex = intArray.Length - 1;

            for (int i = 0; i < intArray.Length; i++)
            {
                if (i % 2 == 0)
                {
                    result[i] = intArray[startIndex].ToString();
                    startIndex++;
                }
                else
                {
                    result[i] = intArray[endIndex].ToString();
                    endIndex--;
                }
            }

            return int.Parse(string.Join("", result));
        }

        public int Solution2(Tree T)
        {
            IList<int> nodes = new List<int>();
            if (T == null)
            {
                return 0;
            }

            var stack = new Stack<Tree>();
            stack.Push(T);

            var visited = new HashSet<Tree>();

            while (stack.Count > 0)
            {
                var current = stack.Peek();
                var left = current.l;

                while (left != null && !visited.Contains(left))
                {
                    stack.Push(left);
                    left = left.l;
                }

                var visit = stack.Pop();

                nodes.Add(visit.x);

                visited.Add(visit);

                if (visit.r != null)
                {
                    stack.Push(visit.r);
                }

            }

            return nodes.Distinct().Count();
        }
    }
}

public class Tree
{
    public int x;
    public Tree l;
    public Tree r;
};
