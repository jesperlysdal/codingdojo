﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace CodingDojo.NamesScores
{
    [TestFixture()]
    public class NamesScoresCalculatorUT
    {
        private NamesScoresCalculator _sut;

        [OneTimeSetUp]
        public void SetUp()
        {
            _sut = new NamesScoresCalculator();
        }

        [Test]
        public void CanSortNamesFileAlphabetically()
        {
            var result = _sut.GetSortedListOfNames();

            Assert.AreEqual("ABBEY", result[0]);
        }

        [Test]
        public void CanGetAlphabeticalValue()
        {
            var result = _sut.GetAlphabeticalValue("ABBEY");

            Assert.AreEqual(35, result);
        }

        [Test]
        public void CanCalculateSumOfNamesScores()
        {
            var result = _sut.SumNamesScores();

            Assert.AreEqual(10, result);
        }

        [Test]
        public void TestSolution()
        {
            var result = _sut.Solution(123456);

            Assert.AreEqual(162534, result);
        }

        [Test]
        public void TestSolutionAgain()
        {
            var result = _sut.Solution(130);

            Assert.AreEqual(103, result);
        }

        [Test]
        public void TestSolution2()
        {
            var result = _sut.Solution2(new Tree(){l = new Tree(){x = 1}, r = new Tree(){x = 3}, x = 1});

            Assert.AreEqual(2, result);
        }
    }
}
