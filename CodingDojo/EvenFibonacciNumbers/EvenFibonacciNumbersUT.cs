﻿using NUnit.Framework;

namespace CodingDojo.EvenFibonacciNumbers
{
    public class EvenFibonacciNumbersUT
    {
        [Test]
        public void CanSumEvenValuedFibonacciNumbers()
        {
            var sut = new EvenFibonacciNumbers();
            var result = sut.SumEvenValuedFibonacciNumbers(below: 4000000);

            Assert.AreEqual(4613732, result);
        }
    }
}
