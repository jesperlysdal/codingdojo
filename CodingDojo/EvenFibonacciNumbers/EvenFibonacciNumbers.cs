﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingDojo.EvenFibonacciNumbers
{
    public class EvenFibonacciNumbers
    {
        public int SumEvenValuedFibonacciNumbers(int below)
        {
            var fibonacciList = new List<int>() {1, 2};
            var pointer = 0;
            var resultList = new List<int>(){2};
            var next = fibonacciList[pointer] + fibonacciList[pointer + 1];

            while(next < below)
            {
                fibonacciList.Add(next);
                if (next % 2 == 0)
                {
                    resultList.Add(next);
                }
                pointer++;
                next = fibonacciList[pointer] + fibonacciList[pointer +1];
            }

            return resultList.Sum();
        }
    }
}
