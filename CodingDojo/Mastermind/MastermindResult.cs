﻿namespace CodingDojo.Mastermind
{
    public enum MastermindResult
    {
        Win,
        Loss,
        InProgress
    }
}