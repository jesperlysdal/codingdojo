﻿namespace CodingDojo.Mastermind
{
    public class Feedback
    {
        public int WellPlaced { get; set; }
        public int Misplaced { get; set; }
        public MastermindResult Result { get; set; }
    }
}