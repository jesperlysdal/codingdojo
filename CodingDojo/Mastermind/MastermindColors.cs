﻿namespace CodingDojo
{
    public enum MastermindColors
    {
        Blue = 1,
        Red = 2,
        Yellow = 3,
        Green = 4,
        White = 5,
        Black = 6,
        Blank = 7
    }
}