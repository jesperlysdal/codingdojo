﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace CodingDojo.Mastermind
{
    public class MastermindGame
    {
        private static List<MastermindColors> _secret;
        public int Round { get; private set; }
        public int RoundLimit { get; private set; }

        public MastermindGame(List<MastermindColors> secret = null, int roundLimit = 10)
        {
            _secret = secret ?? Scramble();
            Round = 0;
            RoundLimit = roundLimit;
        }

        public Feedback Evaluate(List<MastermindColors> guess)
        {
            if (guess.Count != 4)
            {
                throw new InvalidDataException();
            }

            CheckGameState(guess);
            
            var result = GiveFeedback(guess: guess);

            Round++;
            return result;
        }

        private Feedback GiveFeedback(List<MastermindColors> guess)
        {
            var result = new Feedback();
            for (var i = 0; i < guess.Count; i++)
            {
                if (guess[i] == _secret[i])
                {
                    result.WellPlaced++;
                    guess.Remove(guess[i]);
                }
            }

            foreach (var color in guess)
            {
                if (_secret.Contains(color))
                {
                    result.Misplaced++;
                }
            }

            return result;
        }

        private MastermindResult CheckGameState(List<MastermindColors> guess)
        {
            if (guess == _secret)
            {
                return MastermindResult.Win;
            }
            if (Round > RoundLimit)
            {
                return MastermindResult.Loss;
            }

            return MastermindResult.InProgress;
        }

        public List<MastermindColors> Scramble(bool allowDuplicates = false, bool allowBlanks = false)
        {
            _secret = new List<MastermindColors>();

            var numberOfColors = allowBlanks ? 7 : 6;

            while (_secret.Count < 4)
            {
                var next = (MastermindColors) new Random().Next(numberOfColors);

                if (!_secret.Contains(next) || allowDuplicates)
                {
                    _secret.Add(next);
                }
            }

            return _secret;
        }
    }
}