﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace CodingDojo.Mastermind
{
    [TestFixture]
    public class MastermindTest
    {
        private MastermindGame _sut;

        [TestCase(new []{MastermindColors.Black, MastermindColors.Green, MastermindColors.White, MastermindColors.Red})]
        public void Test_Evaluate(MastermindColors[] secret)
        {
            _sut = new MastermindGame(secret.ToList());
            
            var guess = new List<MastermindColors>
                {MastermindColors.Blue, MastermindColors.Green, MastermindColors.Red, MastermindColors.Yellow};
            
            var result = _sut.Evaluate(guess);
            
            Assert.AreEqual(expected: 1, actual: result.WellPlaced);
            Assert.AreEqual(expected: 1, actual: result.Misplaced);
        }

        [TestCase(new[] {MastermindColors.Black, MastermindColors.Green, MastermindColors.White, MastermindColors.Red})]
        public void Test_Scramble_Secret(MastermindColors[] secret)
        {
            _sut = new MastermindGame(secret.ToList());

            var result = _sut.Scramble();
            
            Assert.AreNotEqual(expected: secret, actual: result);
        }

        [TestCase(new[] {MastermindColors.Black, MastermindColors.Green, MastermindColors.White, MastermindColors.Red})]
        public void Test_Counting_Rounds(MastermindColors[] secret)
        {
            _sut = new MastermindGame(secret.ToList());
            
            var guess1 = new List<MastermindColors>
                {MastermindColors.Blue, MastermindColors.Green, MastermindColors.Red, MastermindColors.Yellow};

            var round1 = _sut.Evaluate(guess1);
            
            var guess2 = new List<MastermindColors>
                {MastermindColors.Blue, MastermindColors.Green, MastermindColors.Red, MastermindColors.Yellow};

            var round2 = _sut.Evaluate(guess2);
            
            Assert.AreEqual(expected: 2, actual: _sut.Round);
        }

        [TestCase]
        public void Test_Round_Limit()
        {
            
        }
    }
}