﻿//Write a method int SumBeyond(int k) to find the least n such that the sum of the natural numbers smaller than n exceeds k.

namespace CodingDojo.SumOfNaturalNumbers
{
    public class SumOfNaturalNumbers
    {
        public int SumBeyond(int k)
        {
            var n = 1;
            var sum = 0;

            while (sum <= k)
            {
                sum += n;
                n += 1;
            }

            return n;
        }
    }
}