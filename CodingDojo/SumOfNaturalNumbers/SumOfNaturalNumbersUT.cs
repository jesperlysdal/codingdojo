﻿using NUnit.Framework;

namespace CodingDojo.SumOfNaturalNumbers
{
    [TestFixture]
    public class SumOfNaturalNumbersUT
    {
        [Test]
        public void test()
        {
            var sut = new SumOfNaturalNumbers();

            var result = sut.SumBeyond(5);
            
            Assert.AreEqual(4, result);
        }
    }
}