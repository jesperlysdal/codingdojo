﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingDojo.SumSquareDifference
{
    public class SumSquareDifferenceCalculator
    {
        public int SumSquareDifference(int digits)
        {
            var sumOfSquares = 0;
            var squareOfSums = 0;

            var numbersSummed = 0;

            for (var i = 1; i <= digits; i++)
            {
                sumOfSquares += (int)Math.Pow(i, 2);
                numbersSummed += i;
            }

            squareOfSums = (int) Math.Pow(numbersSummed, 2);

            return squareOfSums - sumOfSquares;
        }
    }
}
