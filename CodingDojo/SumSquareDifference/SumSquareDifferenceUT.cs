﻿using NUnit.Framework;

namespace CodingDojo.SumSquareDifference
{
    [TestFixture]
    public class SumSquareDifferenceUT
    {
        private SumSquareDifferenceCalculator _sut;

        [OneTimeSetUp]
        public void SetUp()
        {
            _sut = new SumSquareDifferenceCalculator();
        }

        [Test]
        public void CanSumSquareDifference()
        {
            var result = _sut.SumSquareDifference(digits: 100);

            Assert.AreEqual(25164150, result);
        }
    }
}
