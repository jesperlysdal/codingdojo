﻿namespace CodingDojo.MontyHallProblem
{
    public static class MontyHallProblem
    {
        public static bool Simulate(
            int pickedDoor, 
            bool changeDoor, 
            int carDoor, 
            int goatDoorToRemove)
        {
            bool win;

            //randomly remove one of the goat doors,
            //but not the one the contestant picked
            var leftGoat = 0;
            var rightGoat = 2;
            switch (pickedDoor)
            {
                case 0: 
                    leftGoat = 1; 
                    rightGoat = 2; 
                    break;
                case 1: 
                    leftGoat = 0; 
                    rightGoat = 2; 
                    break;
                case 2: 
                    leftGoat = 0; 
                    rightGoat = 1; 
                    break;
            }

            var keepGoat = goatDoorToRemove == 0 ? rightGoat : leftGoat;

            if (!changeDoor)
            {
                //not changing the initially picked door
                win = carDoor == pickedDoor;
            }
            else
            {
                //changing picked door to the other door remaining
                win = carDoor != keepGoat;
            }

            return win;
        }
    }
}