﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingDojo.HighlyDivisbleTriangularNumber
{
    public class TriangleNumberDivisorCalculator
    {
        public int Get(int divisors)
        {
            var triangle = 0;

            for (int i = 1;; i++)
            {
                if(int.MaxValue - triangle < i)
                    throw new ArithmeticException("Overflow");

                triangle += i;
                if (CountDivisors(triangle) > 500)
                    return triangle;
            }
        }

        private static int CountDivisors(int number)
        {
            int count = 0;
            var end = Math.Sqrt(number);

            for (int i = 1; i < end; i++)
            {
                if (number % i == 0)
                    count += 2;
            }
            if (end * end == number)
            {
                count++;
            }

            return count;
        }
    }
}
