﻿using NUnit.Framework;

namespace CodingDojo.HighlyDivisbleTriangularNumber
{
    public class TriangleNumberDivisorCalculatorUT
    {
        [Test]
        public void CanFindTriangularNumberWithXDivisors()
        {
            var sut = new TriangleNumberDivisorCalculator();
            var result = sut.Get(divisors: 500);

            Assert.AreEqual(76576500, result);
        }
    }
}
