﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingDojo.LargestProductInGrid
{
    public class LargestProductInGridCalculator
    {
        private static int CONSECUTIVE = 4;

        public int Get(int digits, int[][] grid)
        {
            int max = -1;
            for (int y = 0; y < grid.Length; y++)
            {
                for (int x = 0; x < grid[y].Length; x++)
                {
                    max = Math.Max(Product(x, y, 1, 0, CONSECUTIVE, grid), max);
                    max = Math.Max(Product(x, y, 0, 1, CONSECUTIVE, grid), max);
                    max = Math.Max(Product(x, y, 1, 1, CONSECUTIVE, grid), max);
                    max = Math.Max(Product(x, y, 1, -1, CONSECUTIVE, grid), max);
                }
            }
            return max;
        }

        private int Product(int x, int y, int dx, int dy, int consecutiveNumbers, int[][] grid)
        {
            // First endpoint is assumed to be in bounds. Check if second endpoint is in bounds.
            if (!IsInBounds(x + (consecutiveNumbers - 1) * dx, y + (consecutiveNumbers - 1) * dy, grid))
                return -1;

            int prod = 1;
            for (int i = 0; i < consecutiveNumbers; i++, x += dx, y += dy)
                prod *= grid[y][x];
            return prod;
        }

        private bool IsInBounds(int x, int y, int[][] grid)
        {
            return 0 <= y && y < grid.Length && 0 <= x && x < grid[y].Length;
        }
    }
}
