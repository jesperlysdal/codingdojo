﻿using NUnit.Framework;

namespace CodingDojo.MultiplesOf3And5
{
    [TestFixture]
    public class MultiplesOf3And5CalculatorUT
    {
        public MultiplesCalculator sut;

        [OneTimeSetUp]
        public void Initialize()
        {
            sut = new MultiplesCalculator();
        }

        [Test]
        public void CanCalculateSumOfMultiplesOf3And5Below1000()
        {
            var multiplesOf3And5 = sut.Multiples(of: 3, and: 5, below: 1000);

            Assert.AreEqual(233168, multiplesOf3And5);
        }
    }
}
