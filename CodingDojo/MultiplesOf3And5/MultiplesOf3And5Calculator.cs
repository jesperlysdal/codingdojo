﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace CodingDojo.MultiplesOf3And5
{
    public class MultiplesCalculator
    {
        public int Multiples(int of, int and, int below)
        {
            var sum = 0;
            for (var i = 0; i < below; i++)
            {
                if (i % of == 0 || i % and == 0)
                    sum += i;
            }
            return sum;
        }
    }
}