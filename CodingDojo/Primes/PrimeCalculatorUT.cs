﻿using NUnit.Framework;

namespace CodingDojo.Primes
{
    [TestFixture]
    public class PrimeCalculatorUT
    {
        private PrimeCalculator _sut;

        [OneTimeSetUp]
        public void SetUp()
        {
            _sut = new PrimeCalculator();
        }

        [Test]
        public void CanCalculatePrimeNrX()
        {
            var result = _sut.Get(primeNo: 10001);

            Assert.AreEqual(104743, result);
        }

        [Test]
        public void CanSumAllPrimesBelowX()
        {
            var result = _sut.SumPrimes(below: 2000000);

            Assert.AreEqual(142913828922, result);
        }
    }
}
