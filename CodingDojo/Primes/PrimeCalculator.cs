﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingDojo.Primes
{
    public class PrimeCalculator
    {
        public int Get(int primeNo)
        {
            var listOfPrimes = new List<int>();
            var counter = 1;

            while (listOfPrimes.Count < primeNo)
            {
                if (CheckPrime(counter))
                {
                    listOfPrimes.Add(counter);
                }
                counter++;
            }

            return listOfPrimes.Last();
        }

        public long SumPrimes(long below)
        {
            var listOfPrimes = new List<long>();
            var counter = 1;

            while (counter < below)
            {
                if (CheckPrime(counter))
                {
                    listOfPrimes.Add(counter);
                }
                counter++;
            }
            return listOfPrimes.Sum();
        }

        public bool CheckPrime(int n)
        {
            if (n <= 1)
                return false;
            if (n <= 3)
                return true;
            if (n % 2 == 0 || n % 3 == 0)
                return false;
            var i = 5;
            while (i * i <= n)
            {
                if (n % i == 0 || n % (i + 2) == 0)
                    return false;
                i = i + 6;
            }
            return true;
        }
    }
}
