﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingDojo.SpecialPythagoranTriplet
{
    public class SpecialPythagoranTripletCalculator
    {
        public int PythagoranTripletProduct(int forResult)
        {
            for (var a = 1; a < forResult; a++)
            {
                for (var b = 2; b < forResult; b++)
                {
                    var cSqr = Math.Pow(a, 2) + Math.Pow(b, 2);
                    var c = Math.Sqrt(cSqr);
                    if (a + b + c == forResult)
                    {
                        return a * b * (int)c;
                    }
                }
            }
            return 0;
        }
    }
}
