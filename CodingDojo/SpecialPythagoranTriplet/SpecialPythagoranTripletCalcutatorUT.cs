﻿using CodingDojo.SpecialPythagoranTriplet;
using NUnit.Framework;

namespace CodingDojo.SpecialPyhtagoranTriplet
{
    [TestFixture]
    public class SpecialPythagoranTripletCalcutatorUT
    {
        private SpecialPythagoranTripletCalculator _sut;

        [OneTimeSetUp]
        public void SetUp()
        {
            _sut = new SpecialPythagoranTripletCalculator();
        }

        [Test]
        public void CanCalculatePythagoranTriplet()
        {
            var result = _sut.PythagoranTripletProduct(forResult: 1000);

            Assert.AreEqual(31875000, result);
        }
    }
}
