﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace CodingDojo.LongestCollatzSequence
{
    public class LongestCollatzSequenceCalculator
    {
        

        public int Calculate(BigInteger number)
        {
            var collatzNumbers = new List<BigInteger>();
            collatzNumbers.Add(number);

            while (number > 1)
            {
                if (number % 2 == 0)
                {
                    number = number / 2;
                    collatzNumbers.Add(number);
                }
                else
                {
                    number = (number * 3) + 1;
                    collatzNumbers.Add(number);
                }
            }

            return collatzNumbers.Count;
        }

        public KeyValuePair<int, int> GetLargestChainForNumbersUnder(int below)
        {
            var longestSequence = 0;
            var number = 0;

            while (below > 1)
            {
                var currentSequenceLength = Calculate(below);
                if (currentSequenceLength > longestSequence)
                {
                    longestSequence = currentSequenceLength;
                    number = below;
                }
                below--;
            }
            
            return new KeyValuePair<int,int>(number, longestSequence);
        }
    }
}
