﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace CodingDojo.LongestCollatzSequence
{
    [TestFixture()]
    public class LongestCollatzSequenceCalculatorUT
    {
        private LongestCollatzSequenceCalculator _sut;

        [OneTimeSetUp]
        public void SetUp()
        {
            _sut = new LongestCollatzSequenceCalculator();
        }

        [Test]
        public void CanReturnLengthOfCollatzSequence()
        {
            var result = _sut.Calculate(13);

            Assert.AreEqual(10, result);
        }

        [Test]
        public void CanCalculateLongestCollatzSequenceForNumbersUnderOneMillion()
        {
            var result = _sut.GetLargestChainForNumbersUnder(below: 1000000);

            Assert.AreEqual(837799, result.Key);
        }
    }
}
