﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CodingDojo.RomanNumbers
{
    public class RomanNumberConverter
    {
        private readonly Dictionary<char, int> _romanNumeralToInteger = new Dictionary<char, int>
        {
            { 'M', 1000 },
            { 'D', 500 },
            { 'C', 100 },
            { 'L', 50 },
            { 'X', 10 },
            { 'V', 5 },
            { 'I', 1}
        };
        
        public string ToRoman(int number)
        {
            if (number < 0 || number > 3999) throw new ArgumentOutOfRangeException("Number must be a value between 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + ToRoman(number - 1000);
            if (number >= 900) return "CM" + ToRoman(number - 900);
            if (number >= 500) return "D" + ToRoman(number - 500);
            if (number >= 400) return "CD" + ToRoman(number - 400);
            if (number >= 100) return "C" + ToRoman(number - 100);
            if (number >= 90) return "XC" + ToRoman(number - 90);
            if (number >= 50) return "L" + ToRoman(number - 50);
            if (number >= 40) return "XL" + ToRoman(number - 40);
            if (number >= 10) return "X" + ToRoman(number - 10);
            if (number >= 9) return "IX" + ToRoman(number - 9);
            if (number >= 5) return "V" + ToRoman(number - 5);
            if (number >= 4) return "IV" + ToRoman(number - 4);
            if (number >= 1) return "I" + ToRoman(number - 1);
            throw new ArgumentOutOfRangeException($"{number} is not valid input.");
        }
        
        public int ToInteger(string romanNumeral)
        {
            var numbers = romanNumeral
                .Select(letter => _romanNumeralToInteger[letter])
                .ToArray();

            for (var i = 0; i < numbers.Length; i++)
            {
                if (i + 1 > numbers.Length - 1 || numbers[i + 1] <= numbers[i]) 
                    continue;
                
                numbers[i] = numbers[i + 1] - numbers[i];
                numbers[i + 1] = 0;
                i++;
            }

            return numbers.Sum();
        }
    }
}
