﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace CodingDojo.RomanNumbers
{
    public class RomanNumberConverterUT
    {
        [Test]
        public void IntToRomanConversion()
        {
            var sut = new RomanNumberConverter();
            var result = sut.ToRoman(2490);

            Assert.AreEqual("MMCDXC", result);
        }
    }
}
