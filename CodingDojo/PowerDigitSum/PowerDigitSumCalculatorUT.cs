﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace CodingDojo.PowerDigitSum
{
    [TestFixture()]
    public class PowerDigitSumCalculatorUT
    {
        private PowerDigitSumCalculator _sut;

        [OneTimeSetUp]
        public void Setup()
        {
            _sut = new PowerDigitSumCalculator();
        }

        [Test]
        public void CanCalculatePowerDigitSum()
        {
            var result = _sut.Calculate(number: 2, power: 1000);

            Assert.AreEqual(1083, result);
        }
    }
}
