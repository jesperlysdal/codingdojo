﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace CodingDojo.PowerDigitSum
{
    public class PowerDigitSumCalculator
    {
        public BigInteger Calculate(int number, int power)
        {
            BigInteger result = 0;
            var bigInt = new BigInteger(Math.Pow(number, power));

            while (bigInt != 0)
            {
                result += bigInt % 10;
                bigInt /= 10;
            }

            return result;
        }
    }
}
