//Constructor Detail
//Deck
//public Deck()
//Creates a new deck of cards.
//Method Detail
//next
//public Card next()
//Returns the next card in the deck, removing it.
//Precondition: The deck must not be empty.
//Returns:
//the next card in the deck
//isEmpty
//public boolean isEmpty()
//Checks whether this deck is empty.
//Returns:
//true if this deck is empty


using System;
using System.Linq;

namespace CodingDojo.Deck
{
    public class Deck
    {
        public readonly Card?[] _deck;
        private int _currentCardIndex = 0;
        
        public Deck()
        {
            _deck = Enum
                .GetValues(typeof(Card))
                .Cast<Card?>()
                .ToArray();
            
            ShuffleDeck();
        }

        private void ShuffleDeck()
        {
            var random = new Random();
            for (var i = 0; i < _deck.Length; i++)
            {
                var next = random.Next(12);
                var temp = _deck[i];
                _deck[i] = _deck[next];
                _deck[next] = temp;
            }
        }

        public Card Next()
        {
            var cardToReturn = _deck[_currentCardIndex].Value;
            _deck[_currentCardIndex] = null;
            _currentCardIndex += 1;

            return cardToReturn;
        }

        public bool IsEmpty()
        {
            var cardsCount = 0;
            foreach (var card in _deck)
            {
                if (card != null)
                    cardsCount = cardsCount + 1;
            }

            return cardsCount == 0;
        }
    }
}