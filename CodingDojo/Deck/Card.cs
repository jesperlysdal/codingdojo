namespace CodingDojo.Deck
{
    public enum Card
    {
        Spar1,
        Spar2,
        Spar3,
        Ruder1,
        Ruder2,
        Ruder3,
        Hjerter1,
        Hjerter2,
        Hjerter3,
        Klør1,
        Klør2,
        Klør3,
    }
}