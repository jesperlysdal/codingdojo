﻿using NUnit.Framework;

namespace CodingDojo.SmallestMultiple
{
    [TestFixture]
    public class SmallestMultipleUT
    {
        private SmallestMultipleCalculator _sut;

        [OneTimeSetUp]
        public void SetUp()
        {
            _sut = new SmallestMultipleCalculator();
        }

        [Test]
        public void CanCalculateSmallestPositiveNumberEvenlyDivisible()
        {
            var result = _sut.SmallestMultiple(digits: 20);

            Assert.AreEqual(232792560, result);
        }
    }
}
