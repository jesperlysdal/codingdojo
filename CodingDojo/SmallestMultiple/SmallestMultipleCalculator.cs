﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingDojo.SmallestMultiple
{
    public class SmallestMultipleCalculator
    {
        public int SmallestMultiple(int digits)
        {
            var evenlyDivisible = false;
            var result = 1;

            while (!evenlyDivisible)
            {
                for (var i = 1; i < digits; i++)
                {
                    if (result % i != 0)
                    {
                        evenlyDivisible = false;
                        result++;
                        break;
                    }
                    evenlyDivisible = true;
                }
            }

            return result;
        }
    }
}
