﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingDojo.BingoGame
{
    public class BingoGame
    {
        private List<int> _numbersNotDrawn;
        private List<int> _numbersDrawn;

        public BingoGame(int max)
        {
            NewGame(max);
        }

        public int GetRandomNumber()
        {
            return new Random().Next(0, _numbersNotDrawn.Count);
        }

        public void NewGame(int max)
        {
            _numbersNotDrawn = Enumerable.Range(1, max).ToList();
            _numbersDrawn = new List<int>();
        }

        public int[] Draw(int times = 1)
        {
            var numbers = new List<int>();
            for (int i = 0; i < times; i++)
            {
                numbers.Add(DrawOnce());
            }

            return numbers.ToArray();
        }

        public int DrawOnce()
        {
            var number = _numbersNotDrawn[GetRandomNumber()];
            _numbersNotDrawn.Remove(number);
            _numbersDrawn.Add(number);
            return number;
        }

        public bool HasBingo(int[] playerNumbers)
        {
            return playerNumbers.Length == 5 
                && playerNumbers.All(e => _numbersDrawn.Contains(e));
        }
    }
}
