﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace CodingDojo.BingoGame
{
    [TestFixture()]
    public class BingoGameUT
    {
        private BingoGame _sut;

        [OneTimeSetUp]
        public void SetUp()
        {
            _sut = new BingoGame(90);
        }

        [Test]
        public void CanGetRandomNumberBetween1And90()
        {
            var result = _sut.GetRandomNumber();

            Assert.GreaterOrEqual(result, 1);
            Assert.LessOrEqual(result, 90);
        }

        [Test]
        public void DrawsRandomNumberWithinLimits()
        {
            _sut.NewGame(90);
            var result = _sut.DrawOnce();

            Assert.LessOrEqual(result, 90);
            Assert.GreaterOrEqual(result, 1);
        }

        [Test]
        public void CanDrawAllNumbers()
        {
            var numbers = _sut.Draw(90).OrderBy(e => e).ToArray();

            Assert.AreEqual(Enumerable.Range(1, 90).ToList(), numbers);
        }

        [Test]
        public void CanCheckForBingo()
        {
            _sut.NewGame(90);
            _sut.Draw(90);
            var result = _sut.HasBingo(new[] {1, 15, 20, 33, 54});

            Assert.True(result);

            _sut.NewGame(90);
            _sut.Draw(4);
            var result2 = _sut.HasBingo(new[] {1, 15, 20, 33, 54});

            Assert.False(result2);
        }
    }
}
