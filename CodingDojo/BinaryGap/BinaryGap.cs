//Our problem for the binary gap challenge is basically:
//Expect an integer between 1 and 2,147,483,647
//Convert that integer into its binary value
//Return the length of the largest sequence of zeros in between ones
//Return 0 if no zeros in between ones

using System;
using System.Collections.Generic;
using System.Linq;

namespace CodingDojo.BinaryGap
{
    public static class BinaryGap
    {
        public static int Calculate(int input)
        {
            if (input <= 0)
                return 0;
            
            var binaryNumber = Convert.ToString(input, 2);

            var trailingZeros = binaryNumber.TrailingZeros();
            var ones = binaryNumber.CountOnes();

            if (trailingZeros.Count == 0 || ones < 2)
                return 0;

            if (binaryNumber.EndsWith("0"))
                trailingZeros.RemoveAt(trailingZeros.Count - 1);

            return trailingZeros
                .OrderByDescending(s => s.Length)
                .First()
                .Length;
        }

        private static int CountOnes(this string input)
        {
            return input
                .ToCharArray()
                .Count(number => number == '1');
        }

        private static List<string> TrailingZeros(this string input)
        {
            return input
                .Split('1')
                .ToList();
        }
    }
}