using NUnit.Framework;

namespace CodingDojo.BinaryGap
{
    [TestFixture()]
    public class BinaryGapUT
    {
        [Test]
        [TestCase(32, 0)]
        [TestCase(53, 1)]
        [TestCase(1042, 5)]
        [TestCase(5012, 2)]
        [TestCase(51272 , 4)]
        public void ReturnsLargestBinaryGap(int n, int expectedResult)
        {
            var result = BinaryGap.Calculate(n);

            Assert.AreEqual(expectedResult, result);
        }
    }
}