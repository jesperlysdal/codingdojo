﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace CodingDojo.FactorialDigitSum
{
    public class FactorialDigitSumCalculator
    {
        public BigInteger Factorial(int of)
        {
            BigInteger result = 1;
            while (of >= 1)
            {
                result = result * of;
                of--;
            }

            return result;
        }

        public int Sum(BigInteger of)
        {
            var temp = of
                .ToString()
                .ToCharArray()
                .Select(e => int.Parse(e.ToString()))
                .ToArray();

            return temp.Sum();
        }
    }
}
