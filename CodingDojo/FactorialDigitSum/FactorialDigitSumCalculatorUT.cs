﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace CodingDojo.FactorialDigitSum
{
    [TestFixture()]
    public class FactorialDigitSumCalculatorUT
    {
        private FactorialDigitSumCalculator _sut;

        [OneTimeSetUp]
        public void SetUp()
        {
            _sut = new FactorialDigitSumCalculator();
        }

        [Test]
        public void CanCalculateFactorial()
        {
            var result = _sut.Factorial(10);

            Assert.AreEqual(3628800, result);
        }

        [Test]
        public void CanCalculateSumOfFactorialProduct()
        {
            var factorial100 = _sut.Factorial(100);
            var result = _sut.Sum(factorial100);

            Assert.AreEqual(648, result);
        }
    }
}
