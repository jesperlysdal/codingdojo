﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingDojo.Helpers
{
    public static class DecimalExtensions
    {
        private static readonly CultureInfo CultureInfo = CultureInfo.GetCultureInfo("da-DK");

        public static string PriceFormat(this decimal price)
        {
            return price.ToString("C", CultureInfo);
        }
    }
}
