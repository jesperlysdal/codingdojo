﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingDojo.LargestPrimeFactor
{
    public class LargestPrimeFactorCalculator
    {
        public long LargestPrimeFactor(long of)
        {
            while (true)
            {
                long prime = SmallestFactor(of);
                if (prime < of)
                {
                    of /= prime;
                }
                else
                {
                    break;
                }
            }
            return of;
        }

        private static long SmallestFactor(long number)
        {
            if (number <= 1)
                throw new ArgumentException();

            for (long i = 2, end = (long)Math.Sqrt(number); i <= end; i++)
            {
                if (number % i == 0)
                    return i;
            }
            return number;
        }
    }
}
