﻿using NUnit.Framework;

namespace CodingDojo.LargestPrimeFactor
{
    public class LargestPrimeFactorUT
    {
        private LargestPrimeFactorCalculator _sut;

        [Test]
        public void CanFindLargestPrimeFactor()
        {
            _sut = new LargestPrimeFactorCalculator();
            var result = _sut.LargestPrimeFactor(of: 600851475143);

            Assert.AreEqual(6857, result);
        }
    }
}
