﻿using System;
using CodingDojo.TennisGameRefactoring;

namespace CodingDojo.TennisGameRefactoring
{
    public class TennisGame3 : ITennisGame
    {
        private int _player2Score;
        private int _player1Score;
        private readonly string _player1Name;
        private readonly string _player2Name;

        public TennisGame3(string player1Name, string player2Name)
        {
            _player1Name = player1Name;
            _player2Name = player2Name;
        }

        public string GetScore()
        {
            string variable;
            if (_player1Score < 4 && _player2Score < 4)
            {
                string[] scores = { "Love", "Fifteen", "Thirty", "Forty" };
                variable = scores[_player1Score];
                return _player1Score == _player2Score 
                    ? variable + "-All" 
                    : variable + "-" + scores[_player2Score];
            }

            if (_player1Score == _player2Score)
                return "Deuce";
            variable = _player1Score > _player2Score 
                ? _player1Name 
                : _player2Name;
            return (_player1Score - _player2Score) * (_player1Score - _player2Score) == 1 
                ? "Advantage " + variable 
                : "Win for " + variable;
        }

        public void WonPoint(string playerName)
        {
            if (playerName == "player1")
                _player1Score += 1;
            else
                _player2Score += 1;
        }
    }
}
