﻿using System;

namespace CodingDojo.TennisGameRefactoring
{
    public interface ITennisGame
    {
        void WonPoint(string playerName);
        string GetScore();

    }
}
