﻿using CodingDojo.TennisGameRefactoring;

public class Player
{
    public Player(string name, Score score)
    {
        Name = name;
        Score = score;
    }
    public string Name { get; set; }
    public Score Score { get; set; }
}