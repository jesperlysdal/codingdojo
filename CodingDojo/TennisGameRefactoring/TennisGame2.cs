﻿namespace CodingDojo.TennisGameRefactoring
{
    public class TennisGame2 : ITennisGame
    {
        private readonly Player _player1;
        private readonly Player _player2;

        public TennisGame2(string player1Name, string player2Name)
        {
            _player1 = new Player(player1Name, Score.Love);
            _player2 = new Player(player2Name, Score.Love);
        }

        public string GetScore()
        {
            if (AreEqual())
            {
                return _player1.Score > Score.Forty
                    ? "Deuce"
                    : $"{_player1.Score}-All";
            }
            if (IsEndgame())
            {
                var placements = GetPlayerPlacements();
                return placements[0].Score - placements[1].Score >= 2
                    ? $"Win for {placements[0].Name}"
                    : $"Advantage {placements[0].Name}";
            }
            return $"{_player1.Score}-{_player2.Score}";
        }

        private Player[] GetPlayerPlacements()
        {
            return _player1.Score > _player2.Score 
                ? new[] {_player1, _player2} 
                : new[] {_player2, _player1};
        }

        private bool IsEndgame()
        {
            return _player1.Score > Score.Forty || _player2.Score > Score.Forty;
        }

        private bool AreEqual()
        {
            return _player1.Score == _player2.Score;
        }

        public void WonPoint(string player)
        {
            if (player == _player1.Name)
                _player1.Score++;
            else
                _player2.Score++;
        }
    }
}