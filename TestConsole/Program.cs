﻿using System;
using System.Linq;
using System.Threading;
using DailyCodingProblem.Problem054;
using DailyCodingProblem.Problem219_Connect_4;
using GameOfLife.Assets;
using Website.Models;
using Website.Services;
using Website.UnoGame;

namespace TestConsole
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            // ConnectFour();
            // UnoGame();
        }

        public static void UnoGame()
        {
            var players = new[] {new Player("Dewden"), new Player("Dewden2")};
            
            var game = new UnoGame(players);
            
            game.Run();
        }

        public static void ConnectFour()
        {
            var game = new Problem219();
            
            game.ConnectFourGameLoop();
        }

        public static void Sudoku()
        {
            var test = new Problem054();
            
            var board = new [,]{
                {'.','.','.','7','.','.','3','.','1'},
                {'3','.','.','9','.','.','.','.','.'},
                {'.','4','.','3','1','.','2','7','.'},
                {'.','6','.','4','.','.','5','.','.'},
                {'.','.','.','.','.','.','.','.','.'},
                {'.','.','1','.','.','8','.','4','.'},
                {'.','.','6','.','2','1','.','5','.'},
                {'.','.','.','.','.','9','.','.','8'},
                {'8','.','5','.','.','4','.','.','.'}};
            
            var board2 = new [,]{
                {'5','3','.','.','7','.','.','.','.'},
                {'6','.','.','1','9','5','.','.','.'},
                {'.','9','8','.','.','.','.','6','.'},
                {'8','.','.','.','6','.','.','.','3'},
                {'4','.','.','8','.','3','.','.','1'},
                {'7','.','.','.','2','.','.','.','6'},
                {'.','6','.','.','.','.','2','8','.'},
                {'.','.','.','4','1','9','.','.','5'},
                {'.','.','.','.','8','.','.','7','9'}};
            
            var board3 = new [,]{
                {'1','.','.','.','.','.','.','.','.'},
                {'.','.','.','.','.','.','.','.','.'},
                {'.','.','.','.','.','.','.','.','.'},
                {'.','.','.','.','.','.','.','.','.'},
                {'.','.','.','.','.','.','.','.','.'},
                {'.','.','.','.','.','.','.','.','.'},
                {'.','.','.','.','.','.','.','.','.'},
                {'.','.','.','.','.','.','.','.','.'},
                {'.','.','.','.','.','.','.','.','.'}};
            
            var board4 = new [,]{
                {'1','6','.','.','.','.','.','.','.'},
                {'2','5','.','.','.','.','.','.','6'},
                {'3','4','.','.','.','.','.','.','.'},
                {'4','9','.','.','.','.','.','.','.'},
                {'5','8','.','.','.','.','.','.','.'},
                {'6','7','.','.','.','.','.','.','.'},
                {'7','3','.','.','.','.','.','.','.'},
                {'8','2','.','.','.','.','.','.','.'},
                {'9','1','.','.','.','.','.','.','.'}};

            var result = test.SolveSudoku(board4);

            if (result.IsSolved)
            {
                Print2DArray(result.Board);
            }
            else
            {
                Console.WriteLine("Not solveable.");
            }
            


            var test2 = 2;
        }
        
        public static void Print2DArray<T>(T[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i,j] + "\t");
                }
                Console.WriteLine();
            }
        }
    }
}