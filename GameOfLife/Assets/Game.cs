using System;
using System.Linq;

namespace GameOfLife.Assets
{
    public class Game
    {
        private static CellType[][] _grid;
        private readonly int _rowCount;
        private readonly int _columnCount;
        public Game(int rowCount, int columnCount)
        {
            _grid = new CellType[rowCount][];
            for (var i = 0; i < rowCount; i++)
            {
                _grid[i] = new CellType[columnCount];
            }
            
            _rowCount = rowCount;
            _columnCount = columnCount;
            InitializeGridWithDeadCells(_grid);
        }

        private void InitializeGridWithDeadCells(CellType[][] grid)
        {
            for (var x = 0; x < _rowCount; x++)
            {
                for (var y = 0; y < _columnCount; y++)
                {
                    grid[x][y] = CellType.Dead;
                }
            }
        }

        public CellType[][] GetGrid()
        {
            return _grid;
        }
        
        public void ComputeNextGeneration() 
        {

            var nextGenerationGrid = new CellType[_rowCount][];
            for (var i = 0; i < _rowCount; i++)
            {
                nextGenerationGrid[i] = new CellType[_columnCount];
            }
            
            InitializeGridWithDeadCells(nextGenerationGrid);

            for (var y = 0; y < _rowCount; y++) {

                for (var x = 0; x < _columnCount; x++) {

                    if (CellIsAliveAndHasLessThanTwoLivingNeighbours(y, x)) 
                        nextGenerationGrid[y][x] = CellType.Dead;
                    else if (CellIsAliveAndHasTwoOrThreeLivingNeighbours(y, x)) 
                        nextGenerationGrid[y][x] = CellType.Live;
                    else if (CellIsAliveAndHasMoreThanThreeLivingNeighbours(y, x))
                        nextGenerationGrid[y][x] = CellType.Dead;
                    else if (CellIsDeadAndHasThreeLivingNeighbours(y, x))
                        nextGenerationGrid[y][x] = CellType.Live;
                    else
                        nextGenerationGrid[y][x] = _grid[y][x];
                }
            }

            _grid = (CellType[][]) nextGenerationGrid.Clone();
        }

        public CellType CheckCell(int row, int column)
        {
            return _grid[row][column];
        }

        public void SetCell(int row, int column, CellType type)
        {
            _grid[row][column] = type;
        }

        private int CountLivingNeighbours(int row, int column)
        {
            var cellsToCheck = new []
            {
                new []{row - 1, column - 1},
                new []{row - 1, column},
                new []{row - 1, column + 1},
                new []{row, column + 1},
                new []{row + 1, column + 1},
                new []{row + 1, column},
                new []{row + 1, column - 1},
                new []{row, column - 1}
            };

            return cellsToCheck
                .Count(cell => 
                    IsInTheGrid(cell[0], cell[1]) && 
                    CheckCell(cell[0], cell[1]) == CellType.Live);
        }
        
        private bool IsInTheGrid(int row, int column) 
        {
            return row >= 0 && column >= 0 && row < _rowCount && column < _columnCount;
        }
        
        private bool CellIsDeadAndHasThreeLivingNeighbours(int row, int column) 
        {
            var livingNeighbours = CountLivingNeighbours(row, column);
            return CheckCell(row, column) == CellType.Dead && livingNeighbours == 3;
        }

        private bool CellIsAliveAndHasMoreThanThreeLivingNeighbours(int row, int column) 
        {
            var livingNeighbours = CountLivingNeighbours(row, column);
            return CheckCell(row, column) == CellType.Live && livingNeighbours > 3;
        }

        private bool CellIsAliveAndHasTwoOrThreeLivingNeighbours(int row, int column) 
        {
            var livingNeighbours = CountLivingNeighbours(row, column);
            return CheckCell(row, column) == CellType.Live && (livingNeighbours == 2 || livingNeighbours == 3);
        }

        private bool CellIsAliveAndHasLessThanTwoLivingNeighbours(int row, int column) 
        {
            var neighboursCount = CountLivingNeighbours(row, column);
            return CheckCell(row, column) == CellType.Live && neighboursCount < 2;
        }
    }
}