using NUnit.Framework;

namespace GameOfLife.Assets
{
    [TestFixture]
    public class GameOfLifeUT
    {
        private readonly Game _game;
        
        public GameOfLifeUT()
        {
            _game = new Game(4, 8);
        }
        
        [Test]
        public void check_if_cell_is_live()
        {
            _game.SetCell(1, 1, CellType.Live);
            
            var result = _game.CheckCell(1,1);
            
            Assert.AreEqual(CellType.Live, result);
        }
    }
}