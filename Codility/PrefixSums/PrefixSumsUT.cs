//For example, given the string S = CAGCCTA and arrays P, Q such that:
//
//    P[0] = 2    Q[0] = 4
//    P[1] = 5    Q[1] = 5
//    P[2] = 0    Q[2] = 6
//the function should return the values [2, 4, 1], as explained above.

using NUnit.Framework;

namespace Codility.PrefixSums
{
    [TestFixture]
    public class PrefixSumsUT
    {
        [Test]
        [TestCase("CAGCCTA", new []{2,5,0}, new[]{4,5,6}, new[]{2,4,1})]
        public void test(string str, int[] p, int[] q, int[] expected)
        {
            var result = new PrefixSums().Solution3(str, p, q);
            
            Assert.AreEqual(expected, result);
        }
    }
}