namespace Codility.PrefixSums
{
    public enum Nucleotide
    {
        A = 1,
        C = 2,
        G = 3,
        T = 4
    }
}