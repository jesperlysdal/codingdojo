﻿//A DNA sequence can be represented as a string consisting of the letters A, C, G and T,
//which correspond to the types of successive nucleotides in the sequence.
//Each nucleotide has an impact factor, which is an integer.
//Nucleotides of types A, C, G and T have impact factors of 1, 2, 3 and 4, respectively.
//You are going to answer several queries of the form: What is the minimal impact factor of nucleotides
//contained in a particular part of the given DNA sequence?
//
//The DNA sequence is given as a non-empty string S = S[0]S[1]...S[N-1] consisting of N characters.
//There are M queries, which are given in non-empty arrays P and Q, each consisting of M integers.
//The K-th query (0 ≤ K < M) requires you to find the minimal impact factor of nucleotides contained
//in the DNA sequence between positions P[K] and Q[K] (inclusive).
//
//For example, consider string S = CAGCCTA and arrays P, Q such that:
//
//    P[0] = 2    Q[0] = 4
//    P[1] = 5    Q[1] = 5
//    P[2] = 0    Q[2] = 6
//The answers to these M = 3 queries are as follows:
//
//The part of the DNA between positions 2 and 4 contains nucleotides G and C (twice), whose impact factors are 3 and 2 respectively, so the answer is 2.
//The part between positions 5 and 5 contains a single nucleotide T, whose impact factor is 4, so the answer is 4.
//The part between positions 0 and 6 (the whole string) contains all nucleotides, in particular nucleotide A whose impact factor is 1, so the answer is 1.
//Write a function:
//
//class Solution { public int[] solution(string S, int[] P, int[] Q); }
//
//that, given a non-empty string S consisting of N characters and two non-empty arrays P and Q consisting of M integers,
//returns an array consisting of M integers specifying the consecutive answers to all queries.
//
//Result array should be returned as an array of integers.
//
//For example, given the string S = CAGCCTA and arrays P, Q such that:
//
//    P[0] = 2    Q[0] = 4
//    P[1] = 5    Q[1] = 5
//    P[2] = 0    Q[2] = 6
//the function should return the values [2, 4, 1], as explained above.
//
//Write an efficient algorithm for the following assumptions:
//
//N is an integer within the range [1..100,000];
//M is an integer within the range [1..50,000];
//each element of arrays P, Q is an integer within the range [0..N − 1];
//P[K] ≤ Q[K], where 0 ≤ K < M;
//string S consists only of upper-case English letters A, C, G, T.

using System.Collections.Generic;
using System.Linq;

namespace Codility.PrefixSums
{
    public class PrefixSums
    { 
        private readonly Dictionary<string, int> _letterToValueDic = new Dictionary<string, int>{
            { "A", 1 },
            { "C", 2 },
            { "G", 3 },
            { "T", 4 }
        };
        
        //O(N * M)
        public int[] Solution(string S, int[] P, int[] Q)
        {
            return P
                .Select((t, i) => S
                    .Substring(t, Q[i] - t + 1)
                    .ToString())
                .Select(word => word.ToCharArray()
                    .Select(letter => _letterToValueDic[letter.ToString()])
                    .Concat(new[] {4})
                    .Min())
                .ToArray();
        } 

        //O(N + M)
        public int[] Solution2(string S, int[] P, int[] Q)
        {
            char[] chars = {'A','C','G','T'};

            var matrix = new int[3, S.Length + 1];
            
            for (var i = 0; i <= 2; i++)
            {
                for (var j = 0; j < S.Length; j++)
                {
                    if (S[j] == chars[i]) 
                        matrix[i, j + 1] = matrix[i, j] + 1;
                    else 
                        matrix[i, j + 1] = matrix[i, j];
                }
            }

            //minimum nucleotides in substrings
            var minimums = new int[P.Length];
            
            for (var i = 0; i < P.Length; i++)
            {
                minimums[i] = 4;
                for (var j = 0; j <= 2; j++)
                {
                    var diff = matrix[j, Q[i]+1] - matrix[j, P[i]];
                    
                    if (diff <= 0) 
                        continue;
                    
                    minimums[i] = j + 1;
                    break;
                }
            }

            return minimums;
        }

        //O(N * M)
        public int[] Solution3(string S, int[] P, int[] Q)
        {
            var a = new List<int>();
            var c = new List<int>();
            var g = new List<int>();
            var result = new List<int>();
            var i = 0;

            foreach (var letter in S.ToCharArray())
            {
                if (letter == 'A') {
                    a.Add(i);
                } 
                else if (letter == 'C') {
                    c.Add(i);
                } 
                else if (letter == 'G') {
                    g.Add(i);
                }
                i++;
            }

            for(var j = 0; j < P.Length; j++) 
            {
                if (HasNucleotide(a, P[j], Q[j]))
                {
                    result.Add(1);
                } 
                else if (HasNucleotide(c, P[j], Q[j])) 
                {
                    result.Add(2);
                } 
                else if (HasNucleotide(g, P[j], Q[j])) 
                {
                    result.Add(3);
                } 
                else 
                {
                    result.Add(4);
                }
            }

            return result.ToArray();
        }
        
        private bool HasNucleotide(List<int> list, int start, int end) 
        {
            return list.Any(e => e >= start && e <= end);
        }
    }
}