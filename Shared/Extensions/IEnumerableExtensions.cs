﻿using System.Collections.Generic;
using System.Linq;

namespace Shared.Extensions
{
    public static class IEnumerableExtensions
    {
        public static List<string> SinglePermutations(this string s) => $"{s}".Length < 2 
            ? new List<string> { s } 
            : SinglePermutations(s.Substring(1))
                .SelectMany(x => Enumerable.Range(0, x.Length + 1)
                    .Select((_, i) => x.Substring(0, i) + s[0] + x.Substring(i)))
                .Distinct()
                .ToList();
    }
}