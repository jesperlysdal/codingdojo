﻿using System;
using System.Threading.Tasks;
using CodingDojo;
using CodingDojo.MontyHallProblem;

namespace LargestPalindromeProgram
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            SimulateMontyHallProblem(100000000, false);
        }

        private static void SimulateMontyHallProblem(int iterations = 1000000, bool changeDoor = true)
        {
            if (iterations <= 0)
                throw new ArgumentException("Number of iterations cannot be <= 0");
            
            var random = new Random();
            double wins = 0;
            double losses = 0;

            for (var i = 0; i < iterations; i++)
            {
                var result = MontyHallProblem.Simulate(
                    pickedDoor: random.Next(3), 
                    changeDoor: changeDoor, 
                    carDoor: random.Next(3), 
                    goatDoorToRemove: random.Next(1));

                if (result) 
                    wins++;
                else
                    losses++;
            }

            var winPercent = wins / iterations * 100;
            var lossPercent = losses / iterations * 100;
            
            Console.WriteLine($"Done.\nWins: {wins} ({winPercent}%) Losses: {losses} ({lossPercent}%) Total: {wins+losses}");
            Console.ReadLine();
        }
        
        private static void RunPalindromeCalculator()
        {
            var palindromeCalculator = new PalindromeCalculator();
            int digits;
            Console.WriteLine("Palindrome Calculator 5000");
            Console.Write("\nPalindrome Calculator 5000 can calculate the");
            Console.Write("\nlargest palindrome product of two factors of X digits.");
            Console.Write("\nHow many digits do you wish to know the largest palindrome");
            Console.Write("\nproduct for? (Enter Q to quit)");


            if (int.TryParse(Console.ReadLine(), out digits))
            {
                var largestPalindrome = palindromeCalculator.GetLargestPalindrome(digits);
                Console.WriteLine($"The largest palindrome of two {digits} digit numbers");
                Console.WriteLine($"is: {largestPalindrome.Value}. The two factors are {largestPalindrome.OperatorA} and {largestPalindrome.OperatorB}");
            }

            Console.Read();
            
        }
    }
}
