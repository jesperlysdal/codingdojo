﻿using System;
using System.Linq;

namespace Calculator
{
    public class Program
    {

        public static void Main(string[] args)
        {
            Console.WriteLine("This is Calculator 5000");
            Console.WriteLine("Enter Q to quit.");
            while (true)
            {
                Console.WriteLine("Type your calculation:");
                string input = Console.ReadLine();

                if (input == "Q")
                {
                    return;
                }

                var firstIteration = true;
                var negativeResult = false;
                var result = 0;

                if (!string.IsNullOrEmpty(input))
                {
                    input = input
                        .Replace("--", "+")
                        .Replace("+-", "-")
                        .Replace("-+", "-");
                }
                
                if (input.Contains("*-") || input.Contains("/-"))
                {
                    var negativeDivisionOccurances = input.Split(new[] {"/-"}, StringSplitOptions.None);
                    var negativeMultiplicationOccurances = input.Split(new[] {"*-"}, StringSplitOptions.None);
                    if (negativeDivisionOccurances.Length + negativeMultiplicationOccurances.Length % 2 == 1)
                    {
                        negativeResult = true;
                    }

                    input = input
                        .Replace("/-", "/")
                        .Replace("*-", "*");
                }

                var numbers = input
                    .Split(new[] {"+", "-", "*", "/"}, StringSplitOptions.None)
                    .Select(int.Parse)
                    .ToList();

                var operators = input
                    .ToCharArray()
                    .Where(e => !Char.IsDigit(e))
                    .Select(e => e.ToString())
                    .ToList();

                while (operators.Contains("/"))
                {
                    var index = operators.IndexOf("/");
                    if (firstIteration)
                    {
                        result = numbers[index] / numbers[index + 1];
                    }
                    else
                    {
                        result = result / numbers[index + 1];
                    }
                    operators[index] = "";
                    firstIteration = false;
                }

                while (operators.Contains("*"))
                {
                    var index = operators.IndexOf("*");
                    if (firstIteration)
                    {
                        result = numbers[index] * numbers[index + 1];
                    }
                    else
                    {
                        result = result * numbers[index + 1];
                    }
                    operators[index] = "";
                    firstIteration = false;
                }

                while (operators.Contains("+"))
                {
                    var index = operators.IndexOf("+");
                    if (firstIteration)
                    {
                        result = numbers[index] + numbers[index + 1];
                    }
                    else
                    {
                        result = result + numbers[index + 1];
                    }
                    operators[index] = "";
                    firstIteration = false;
                }

                while (operators.Contains("-"))
                {
                    var index = operators.IndexOf("-");
                    if (firstIteration)
                    {
                        result = numbers[index] - numbers[index + 1];
                    }
                    else
                    {
                        result = result - numbers[index + 1];
                    }
                    operators[index] = "";
                    firstIteration = false;
                }

                if (negativeResult)
                {
                    result = -result;
                }

                Console.WriteLine($"Result: {result}");
            }
        }
    }
}
